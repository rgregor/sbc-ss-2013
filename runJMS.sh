#!/bin/bash
mvn package

# Run all the brokers
# nameservice + management (hardcoded)
mvn exec:java -Dexec.mainClass="org.apache.activemq.console.Main" -Dexec.args="start broker:(tcp://localhost:61610)?useJmx=true&persistent=false" &

# failunsafe pizzatarget
mvn exec:java -Dexec.mainClass="org.apache.activemq.console.Main" -Dexec.args="start broker:(tcp://localhost:61611)?useJmx=true&persistent=false" &

#pizzaria 1
mvn exec:java -Dexec.mainClass="org.apache.activemq.console.Main" -Dexec.args="start broker:(tcp://localhost:61621)?useJmx=true&persistent=false" &

#pizzaria 2
mvn exec:java -Dexec.mainClass="org.apache.activemq.console.Main" -Dexec.args="start broker:(tcp://localhost:61622)?useJmx=true&persistent=false" &


# wait for broakers to start
sleep 60
echo "*** continuing launch process ***"
echo "*** continuing launch process ***"
echo "*** continuing launch process ***"
echo "*** continuing launch process ***"
echo "*** continuing launch process ***"


# Run Groupgui
#mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.group.GroupAgent" -Dexec.args="JMS tcp://localhost:61610?jms.prefetchPolicy.all=1" &

#run pizzeria1
./runJMSPizzaria.sh "tcp://localhost:61621?jms.prefetchPolicy.all=1"
./runJMSPizzaria.sh "tcp://localhost:61622?jms.prefetchPolicy.all=1"

