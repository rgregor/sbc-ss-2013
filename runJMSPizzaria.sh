#!/bin/bash
echo "running pizzaria: $1"

mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.pizzeria.PizzeriaAgent" -Dexec.args="JMS $1" &
mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.waiter.WaiterAgent" -Dexec.args="JMS $1 1" &
mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.waiter.WaiterAgent" -Dexec.args="JMS $1 2" &
mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.cook.CookAgent" -Dexec.args="JMS $1 3" &
mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.cook.CookAgent" -Dexec.args="JMS $1 4" &
mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.driver.DriverAgent" -Dexec.args="JMS $1 5" &
mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.driver.DriverAgent" -Dexec.args="JMS $1 6" &
