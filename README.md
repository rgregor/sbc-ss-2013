Space Based Computing SS 2013
==================
### How to start the JMS Group GUI
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.group.GroupAgent" -Dexec.args="XVSM"
		
### How to start the JMS Pizzeria GUI
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.pizzeria.PizzeriaAgent" -Dexec.args="JMS"

### How to start a JMS Waiter with Id 1
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.jms.waiter.Waiter" -Dexec.args="1"

### How to start a JMS Cook with Id 1
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.jms.cook.Cook" -Dexec.args="1"
		

XVSM
----

The Group Agent always expects a running space on 9876 and 9877!
The Load Balancer also expects a running space (given as argument) as well as waiters, cooks, pizzerias and drivers.

Start the Spaces first and then the actors!

After the Pizzeria starts it registers itself at the Group Agent. Then the pizzeria can be selected on group creation.

### 1. Start the Group Space Server
		mvn exec:java -Dexec.mainClass="org.mozartspaces.core.Server" -Dexec.args=9876

### 2. Start the Customers' Space
		mvn exec:java -Dexec.mainClass="org.mozartspaces.core.Server" -Dexec.args=9877

### 3. Start the Group Space GUI
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.group.GroupAgent" -Dexec.args="XVSM"

### Start a Pizzeria GUI
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.pizzeria.PizzeriaAgent" -Dexec.args="XVSM <PORT>"

### Start a Space Waiter
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.xvsm.waiter.Waiter" -Dexec.args="<ID> <Pizzeria-PORT>"

### Start a Space Cook
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.xvsm.cook.Cook" -Dexec.args="<ID> <Pizzeria-PORT>"

### Start a Space Driver
		mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.xvsm.driver.Driver" -Dexec.args="<ID> <Pizzeria-PORT>"

### Start a Load Balancer
        mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.xvsm.loadbalancer.LoadBalancer" -Dexec.args="<ID> <PORT>"


As an example three files are included in the Root-directory: runXVSM-spaces.sh, runXVSM.sh, runXVSM-group.sh, runXVSM-loadbalancer.sh
Run them in that order.