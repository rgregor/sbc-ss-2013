package at.ac.tuwien.sbc.valesriegler.xvsm.cook;


import at.ac.tuwien.sbc.valesriegler.common.Tuple;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.xvsm.CookXVSM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Cook {
	private static final String USAGE = "Cook needs exactly two integer parameters: COOKID, PIZZERIA-SPACE-PORT";
	private static final Logger log = LoggerFactory.getLogger(Cook.class);

    private final int pizzeriaPort;
    private final int id;
    private CookXVSM xvsm;

	public static void main(String[] args) {
        final Tuple<Integer> cookIdAndSpacePort = Util.parseIdAndSpacePort(args, USAGE);

		Cook cook = new Cook(cookIdAndSpacePort.fst, cookIdAndSpacePort.snd);
		cook.start();
	}

    private void start() {
        xvsm = new CookXVSM(id, pizzeriaPort);
        xvsm.listenForPizzas();
        xvsm.listenForDeliveryPizzas();
        xvsm.start();
    }

	public Cook(int id, int pizzeriaPort) {
		this.id = id;
        this.pizzeriaPort = pizzeriaPort;
		log.info("I AM A Cook WITH ID {}", id);
	}

}
