package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractDeliveryAction;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;

/**
 * This is the phonecall to a pizzeria.
 * 
 * @author jan
 * 
 */
public class DeliveryOrderResponse extends AbstractDeliveryAction implements Serializable {
	private final int waiterId;

	public DeliveryOrderResponse(DeliveryGroupData deliveryGroupData, int waiterId) {
		super(deliveryGroupData);
		this.waiterId = waiterId;
	}

	@Override
	public String toString() {
		return "DeliveryOrderResponse [waiterId=" + waiterId + ", getDeliveryGroupData()=" + getDeliveryGroupData() + "]";
	}

	public int getWaiterId() {
		return waiterId;
	}

}
