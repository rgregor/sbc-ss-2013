package at.ac.tuwien.sbc.valesriegler.pizzeria.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.Table;

/**
 * Indicate the creation of a new table.
 * 
 * @author jan
 * 
 */
public class TableNew extends AbstractAction implements Serializable {
	private final Table table;

	public TableNew(Table table) {
		super(null);
		this.table = table;
	}

	public Table getTable() {
		return table;
	}

	@Override
	public String toString() {
		return "TableNew [table=" + table + "]";
	}

}
