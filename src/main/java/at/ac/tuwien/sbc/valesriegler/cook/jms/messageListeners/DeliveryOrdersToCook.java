package at.ac.tuwien.sbc.valesriegler.cook.jms.messageListeners;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.cook.actions.DeliveryOrderInfo;
import at.ac.tuwien.sbc.valesriegler.cook.jms.JMSCook;
import at.ac.tuwien.sbc.valesriegler.group.actions.DeliveryOrderRequest;
import at.ac.tuwien.sbc.valesriegler.types.OrderStatus;
import at.ac.tuwien.sbc.valesriegler.types.Pizza;
import at.ac.tuwien.sbc.valesriegler.types.PizzaOrder;
import at.ac.tuwien.sbc.valesriegler.types.PizzaOrderStatus;

/**
 * Cook the requested pizza.
 * 
 * @author jan
 * 
 */
public class DeliveryOrdersToCook implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(DeliveryOrdersToCook.class);
	private final JMSCook cook;

	public DeliveryOrdersToCook(JMSCook cook) {
		this.cook = cook;
	}

	@Override
	public void onMessage(Message msg) {
		try {
			synchronized (cook) {
				if (msg instanceof ObjectMessage) {
					ObjectMessage objMsg = (ObjectMessage) msg;
					Object obj = objMsg.getObject();

					if (obj instanceof DeliveryOrderRequest) {
						DeliveryOrderRequest dor = (DeliveryOrderRequest) obj;
						log.debug("Received: " + dor);

						for (PizzaOrder pizzaorder : dor.getDeliveryGroupData().getOrder().getOrderedPizzas()) {
							pizzaorder.setStatus(PizzaOrderStatus.IN_PREPARATION);
							pizzaorder.setCookId(cook.getId());
						}

						ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(cook.getCONNECTSTRING());
						Connection connection = connectionFactory.createConnection();
						connection.start();

						// inform pizzeria
						Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
						MessageProducer prod = session.createProducer(session.createQueue("PizzeriaConnector"));
						prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						DeliveryOrderInfo doi = new DeliveryOrderInfo(dor.getDeliveryGroupData(), cook.getId());
						prod.send(session.createObjectMessage(doi));
						session.close();

						// generate delay
						for (PizzaOrder po : dor.getDeliveryGroupData().getOrder().getOrderedPizzas()) {
							if (!Util.runSimulation) {
								Thread.sleep(po.getPizzaType().duration * 1000);
							}
							po.setStatus(PizzaOrderStatus.DONE);
							Pizza p = Pizza.createPizzaFromPizzaOrder(po, cook.getId(), false);
							dor.getDeliveryGroupData().getOrder().getCookedPizzas().add(p);
						}
						dor.getDeliveryGroupData().getOrder().setStatus(OrderStatus.DELIVERY_PENDING);

						// let pizzas be delivered.
						session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
						prod = session.createProducer(session.createQueue("CookedDeliveryOrders"));
						prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						prod.send(session.createObjectMessage(doi));

						// inform pizzeria
						prod = session.createProducer(session.createQueue("PizzeriaConnector"));
						prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						// WTF: oi = new OrderInfo(orderrequest.getGroupdata(), cook.getId());
						prod.send(session.createObjectMessage(doi));
						session.close();

						// inform GroupGUI
						prod = session.createProducer(session.createQueue("GroupConnector"));
						prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						// WTF: oi = new OrderInfo(orderrequest.getGroupdata(), cook.getId());
						prod.send(session.createObjectMessage(doi));
						session.close();
						connection.close();
					} else {
						log.warn("Received unknown Object: " + obj);
					}
				} else {
					log.warn("Received unknown Message: " + msg);
				}
				msg.acknowledge();
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		} catch (InterruptedException e) {
			log.error("EXCEPTION!", e);
		}
	}

	@Override
	public String toString() {
		return "CookRequestedPizza [cook=" + cook + "]";
	}
}
