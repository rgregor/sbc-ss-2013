package at.ac.tuwien.sbc.valesriegler.xvsm;

public class EntityNotFoundByTemplate extends RuntimeException {

	public EntityNotFoundByTemplate() {
	}

	public EntityNotFoundByTemplate(String message) {
		super(message);
	}
}
