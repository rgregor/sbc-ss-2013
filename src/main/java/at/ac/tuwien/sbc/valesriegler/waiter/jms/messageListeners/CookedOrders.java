package at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.cook.actions.OrderInfo;
import at.ac.tuwien.sbc.valesriegler.types.OrderStatus;
import at.ac.tuwien.sbc.valesriegler.waiter.actions.DeliverOrder;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.JMSWaiter;

/**
 * Listener listening on the CookedOrders MQ, handling all incomming messages.
 * 
 * @author jan
 * 
 */
public class CookedOrders implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(CookedOrders.class);
	private final JMSWaiter waiter;

	public CookedOrders(JMSWaiter waiter) {
		this.waiter = waiter;
	}

	@Override
	public void onMessage(Message msg) {
		try {
			synchronized (waiter) {
				msg.acknowledge();
				if (msg instanceof ObjectMessage) {
					ObjectMessage objMsg = (ObjectMessage) msg;
					Object obj = objMsg.getObject();

					if (obj instanceof OrderInfo) {
						OrderInfo orderinfo = (OrderInfo) obj;
						log.debug("Received: " + orderinfo);

						// generate random delay
						if (!Util.runSimulation) {
							Thread.sleep((long) (Math.random() * 10000));
						}

						ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(waiter.getCONNECTSTRING());
						Connection connection = connectionFactory.createConnection();
						connection.start();
						Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

						// inform Group + Pizzeria
						MessageProducer informGroup = session.createProducer(session.createQueue("GroupConnector"));
						informGroup.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						DeliverOrder deliverorder = new DeliverOrder(orderinfo.getGroupdata(), waiter.getId());
						deliverorder.getGroupdata().getOrder().setStatus(OrderStatus.DELIVERED);
						informGroup.send(session.createObjectMessage(deliverorder));

						MessageProducer informPizzeria = session.createProducer(session.createQueue("PizzeriaConnector"));
						informPizzeria.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						informPizzeria.send(session.createObjectMessage(deliverorder));

						session.close();
						connection.close();
					} else {
						log.warn("Received unknown Object: " + obj);
					}
				} else {
					log.warn("Received unknown Message: " + msg);
				}
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		} catch (InterruptedException e) {
			log.error("EXCEPTION!", e);
		}
	}
}
