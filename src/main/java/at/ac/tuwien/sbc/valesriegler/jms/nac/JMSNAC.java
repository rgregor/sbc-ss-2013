package at.ac.tuwien.sbc.valesriegler.jms.nac;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.AbstractNACMsg;

/**
 * JMS "Naming And Control". This class registers the listener and initializes the Broker connection.
 * 
 * @author jan
 * 
 */
public class JMSNAC {
	private static final Logger log = LoggerFactory.getLogger(JMSNAC.class);
	public static final String JMS_NAC_SERVICE = "tcp://localhost:61610?jms.prefetchPolicy.all=1";
	public static final String JMS_NAC_TOPIC = "NAC";
	private Connection connection;

	public JMSNAC(AbstractJMSNACMsgListener listener) {
		log.info("Initialising Naming and control module.");
		listener.setJmsnac(this);

		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(JMS_NAC_SERVICE);
			connection = connectionFactory.createConnection();
			connection.start();

			Session sessWantToSit = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consWantToSit = sessWantToSit.createConsumer(sessWantToSit.createTopic(JMS_NAC_TOPIC));
			consWantToSit.setMessageListener(listener);
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}
	public void sendNACMsg(AbstractNACMsg msg) throws JMSException {
		log.debug("sendNACMsg():" + msg);
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		MessageProducer prod = session.createProducer(session.createTopic(JMS_NAC_TOPIC));
		prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		prod.send(session.createObjectMessage(msg));
		session.close();
	}

	public void stop() {
		try {
			connection.close();
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}
}
