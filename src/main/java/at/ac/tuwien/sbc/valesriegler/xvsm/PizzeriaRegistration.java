package at.ac.tuwien.sbc.valesriegler.xvsm;


import java.io.Serializable;

/**
 * {@link PizzeriaRegistration} objects are written to the space to indicate the existance of a new pizzeria at the
 * port specified by {@link PizzeriaRegistration#pizzeriaSpacePort}
 */
public class PizzeriaRegistration implements Serializable {
    public final int pizzeriaSpacePort;

    public PizzeriaRegistration(int pizzeriaSpacePort) {
        this.pizzeriaSpacePort = pizzeriaSpacePort;
    }
}
