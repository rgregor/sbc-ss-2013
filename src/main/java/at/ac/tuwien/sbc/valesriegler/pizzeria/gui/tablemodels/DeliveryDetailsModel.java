package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;


import at.ac.tuwien.sbc.valesriegler.common.TableModel;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;

public class DeliveryDetailsModel extends TableModel<DeliveryGroupData> {
    private static final String WAITER_ID = "Waiter ID";
    private static final String DRIVER_ID = "Driver ID";
    private static final String[] COLUMNS = new String[] {WAITER_ID, DRIVER_ID};

    protected DeliveryGroupData groupData;

    public void setCurrentDelivery(DeliveryGroupData groupData) {
        this.groupData = groupData;

        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return groupData == null ? 0 : 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex > 0)
            return null;

        String wantedColumn = COLUMNS[columnIndex];
        switch (wantedColumn) {
            case WAITER_ID:
                return Util.getId(groupData.getWaiterIdOfOrder());
            case DRIVER_ID:
                return Util.getId(groupData.getDriverId());
            default:
                throw new RuntimeException(UNHANDLEDCOLUMN);
        }
    }

    @Override
    protected String[] getColumns() {
        return COLUMNS;
    }

    public DeliveryGroupData getCurrentDelivery() {
        return groupData;
    }
}
