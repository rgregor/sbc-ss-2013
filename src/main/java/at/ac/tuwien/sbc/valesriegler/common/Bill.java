package at.ac.tuwien.sbc.valesriegler.common;


import java.io.Serializable;

public class Bill implements Serializable {
    public final double amount;
    public final int groupId;

    public Bill(double amount, int groupId) {
        this.amount = amount;
        this.groupId = groupId;
    }
}
