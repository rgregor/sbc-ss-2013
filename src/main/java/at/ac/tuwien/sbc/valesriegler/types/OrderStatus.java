package at.ac.tuwien.sbc.valesriegler.types;

import java.io.Serializable;

/**
 * Enum denoting all the possible states an Order can have.
 * 
 * @author jan
 * 
 */
public enum OrderStatus implements Serializable {
	NEW, ORDERED, DELIVERY_PENDING, DELIVERED, PAID
}
