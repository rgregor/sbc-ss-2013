package at.ac.tuwien.sbc.valesriegler.pizzeria;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;

/**
 * Abstract class to handle lower-level communication with other processes.
 * 
 * @author jan
 * 
 */
public abstract class AbstractPizzeriaConnector {
	public void init() {
	}

	public void send(AbstractAction request) {
	}

}
