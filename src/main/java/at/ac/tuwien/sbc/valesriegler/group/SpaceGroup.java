package at.ac.tuwien.sbc.valesriegler.group;

import at.ac.tuwien.sbc.valesriegler.xvsm.GroupXVSM;

public class SpaceGroup implements Runnable {
	private GroupXVSM xvsm;
	
	public SpaceGroup(int groupId, int pizzeriaSpacePort) {
		xvsm = new GroupXVSM(groupId, pizzeriaSpacePort);
	}

	@Override
	public void run() {
		xvsm.waitForMyOrder();
	}

}
