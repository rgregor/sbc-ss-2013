package at.ac.tuwien.sbc.valesriegler.jms.nac;

import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract "Naming and Control"-msgListener.
 * 
 * @author jan
 * 
 */
public abstract class AbstractJMSNACMsgListener implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(AbstractJMSNACMsgListener.class);
	private JMSNAC jmsnac = null;

	public AbstractJMSNACMsgListener() {
	}
	public void setJmsnac(JMSNAC jmsnac) {
		log.info("setJmsnac():" + jmsnac);
		this.jmsnac = jmsnac;
	}
	public JMSNAC getJmsnac() {
		return jmsnac;
	}
}
