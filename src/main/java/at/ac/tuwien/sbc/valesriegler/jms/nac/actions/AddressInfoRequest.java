package at.ac.tuwien.sbc.valesriegler.jms.nac.actions;


/**
 * Message hat instructs everyone to reply their Address.
 * 
 * @author jan
 * 
 */
public class AddressInfoRequest extends AbstractNACMsg {
	public AddressInfoRequest() {
	}

	@Override
	public String toString() {
		return "AddressInfoRequest []";
	}
}
