package at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.group.actions.PayRequest;
import at.ac.tuwien.sbc.valesriegler.group.actions.PayResponse;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.JMSWaiter;

/**
 * Listener listening on the WantToPay MQ, handling all incomming messages.
 * 
 * @author jan
 * 
 */
public class WantToPay implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(WantToPay.class);
	private final JMSWaiter waiter;

	public WantToPay(JMSWaiter waiter) {
		this.waiter = waiter;
	}

	@Override
	public void onMessage(Message msg) {
		try {
			synchronized (waiter) {
				msg.acknowledge();
				if (msg instanceof ObjectMessage) {
					ObjectMessage objMsg = (ObjectMessage) msg;
					Object obj = objMsg.getObject();

					if (obj instanceof PayRequest) {
						PayRequest payrequest = (PayRequest) obj;
						log.debug("Received: " + payrequest);

						// generate random delay
						if (!Util.runSimulation) {
							Thread.sleep((long) (Math.random() * 10000));
						}

						ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(waiter.getCONNECTSTRING());
						Connection connection = connectionFactory.createConnection();
						connection.start();
						Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
						PayResponse pr = new PayResponse(payrequest.getGroupdata(), waiter.getId());

						MessageProducer informGroup = session.createProducer(session.createQueue("GroupConnector"));
						informGroup.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						informGroup.send(session.createObjectMessage(pr));

						MessageProducer informPizzera = session.createProducer(session.createQueue("PizzeriaConnector"));
						informPizzera.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						informPizzera.send(session.createObjectMessage(pr));

						session.close();
						connection.close();
					} else {
						log.warn("Received unknown Object: " + obj);
					}
				} else {
					log.warn("Received unknown Message: " + msg);
				}
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		} catch (InterruptedException e) {
			log.error("EXCEPTION!", e);
		}
	}
}
