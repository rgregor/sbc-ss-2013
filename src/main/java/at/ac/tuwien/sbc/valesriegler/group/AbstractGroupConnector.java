package at.ac.tuwien.sbc.valesriegler.group;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;

/**
 * Abstract class to handle lower-level communication with other processes.
 * 
 * @author jan
 * 
 */
public abstract class AbstractGroupConnector {
	public void init() {
	}

	public void send(AbstractAction request) {
	}

}
