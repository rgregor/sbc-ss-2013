package at.ac.tuwien.sbc.valesriegler.group.gui;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

/**
 * Base Frame of the Group UI
 * 
 * @author Gregor Riegler <gregor DOT riegler AT gmail DOT com>
 * 
 */
@SuppressWarnings("serial")
public class GroupFrame extends JFrame {
	private static final Logger log = LoggerFactory.getLogger(GroupFrame.class);

	public GroupFrame(GroupOverviewModel groupModel, DeliveryOverviewModel deliveryModel) {
		super("Groups");
		log.debug("GroupFrame()");
		JPanel wrapper = new JPanel();
		GridLayout wrapperLayout = new GridLayout(2, 2);
		wrapper.setLayout(wrapperLayout);

		// Create Wizard-like panel for the creation of groups
		wrapper.add(new GroupCreationPanel(false));

		// Create Wizard-like panel for the creation of delivery groups
		wrapper.add(new GroupCreationPanel(true));

		// Create the panel for the group overview table
		initGroupOverview(wrapper, groupModel);

		// Create the panel for the deliveries overview table
		initDeliveriesOverview(wrapper, deliveryModel);

		setContentPane(wrapper);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        this.setMaximizedBounds(env.getMaximumWindowBounds());
        this.setExtendedState(this.getExtendedState() | this.MAXIMIZED_BOTH);
	}

	private void initGroupOverview(JPanel wrapper, GroupOverviewModel groupModel) {
		JPanel tablePanel = new JPanel();
		Util.createTableInTitledPanel(tablePanel, groupModel, "Group Overview");
		wrapper.add(tablePanel);
	}

	private void initDeliveriesOverview(JPanel wrapper, DeliveryOverviewModel deliveryModel) {
		JPanel tablePanel = new JPanel();
		Util.createTableInTitledPanel(tablePanel, deliveryModel, "Deliveries Overview");
		wrapper.add(tablePanel);
	}

}
