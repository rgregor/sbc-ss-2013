package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;

import at.ac.tuwien.sbc.valesriegler.common.TableModel;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.types.Order;
import at.ac.tuwien.sbc.valesriegler.types.Pizza;
import at.ac.tuwien.sbc.valesriegler.types.PizzaOrder;
import at.ac.tuwien.sbc.valesriegler.types.PizzaOrderStatus;

import java.util.List;

public class PizzasOfOrderModel extends TableModel<Order> {
	private static final String TYPE = "Type";
	private static final String STATUS = "Status";
	private static final String COOK = "Cook";

	private static final String[] COLUMNS = new String[] { TYPE, STATUS, COOK };

    private Order currentOrder;

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<PizzaOrder> pizzaOrders = currentOrder.getOrderedPizzas();
		List<Pizza> pizzas = currentOrder.getCookedPizzas();

		PizzaOrder pizzaOrder = pizzaOrders.get(rowIndex);
		String wantedColumn = COLUMNS[columnIndex];
		switch (wantedColumn) {
		case TYPE:
			return pizzaOrder.getPizzaType();
		case STATUS:
			return pizzaOrder.getStatus();
		case COOK:
			if (pizzaOrder.getStatus() == PizzaOrderStatus.DONE && Util.useJMS) {
				Pizza pizza = pizzas.get(rowIndex);
				return Util.getId(pizza.getCookId());
			}
			else if(!Util.useJMS) return Util.getId(pizzaOrder.getCookId());
			return "";
		default:
			throw new RuntimeException(UNHANDLEDCOLUMN);
		}
	}

    public void setCurrentOrder(Order currentOrder) {
        this.currentOrder = currentOrder;

        fireTableDataChanged();
    }

    public Order getCurrentOrder() {
        return currentOrder;
    }

	@Override
	protected String[] getColumns() {
		return COLUMNS;
	}

	@Override
	public int getRowCount() {
		return currentOrder == null ? 0 : currentOrder.getOrderedPizzas().size();
	}

}
