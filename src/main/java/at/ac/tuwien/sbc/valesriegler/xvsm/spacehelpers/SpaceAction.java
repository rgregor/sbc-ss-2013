package at.ac.tuwien.sbc.valesriegler.xvsm.spacehelpers;


import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SpaceAction {
    protected AtomicBoolean inNotification = new AtomicBoolean(true);

    public abstract void onEntriesWritten(List<? extends Serializable> entries) throws Exception;
}
