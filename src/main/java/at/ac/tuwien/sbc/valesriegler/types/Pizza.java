package at.ac.tuwien.sbc.valesriegler.types;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.JMSWaiter;

import java.io.Serializable;

/**
 * this is a PIZZA! omnomnomnom
 * 
 * @author jan
 * 
 */
public class Pizza extends PizzaOrder implements Serializable {
	public JMSWaiter deliveryAgent;
    protected Boolean isDeliveryPizza;

    /**
	 * Yes that's funny! the pizza has a field idOfOrder although PizzaOrder
	 * already has a field orderId! The reason: The space linda selection is a
	 * little bit limited. when I look for a template pizza with orderId set the
	 * space gives me ALL pizzas. The reason obviously is that it can't really
	 * include the field of the superclass in the search. So we must have an
	 * idOfOrder field in the pizza and not only in the PizzaOrder for the time
	 * being.. ^^
	 */
	private Integer idOfOrder;

	private Pizza(int id, PizzaType type, int cookId, int orderId, boolean isDeliveryPizza) {
		this.id = id;
		this.pizzaType = type;
		this.cookId = cookId;
		this.idOfOrder = orderId;
        this.isDeliveryPizza = isDeliveryPizza;
		this.status = PizzaOrderStatus.DONE;
	}

	public Pizza() {
		super();
	}

	public static Pizza createPizzaFromPizzaOrder(PizzaOrder pizzaorder, int cookId, boolean isDeliveryPizza) {
		return new Pizza(pizzaorder.getId(), pizzaorder.getPizzaType(), cookId, pizzaorder.getOrderId(), isDeliveryPizza);
	}

	@Override
	public String toString() {
		return "Pizza [deliveryAgent=" + deliveryAgent + ", id=" + id + ", orderId=" + orderId + ", pizzaType=" + pizzaType
				+ ", status=" + status + ", cookId=" + cookId + "]";
	}

	@Override
	public int getOrderId() {
		return getIdOfOrder();
	}

	@Override
	public void setOrderId(Integer orderId) {
		idOfOrder = orderId;
	}

	public int getIdOfOrder() {
		return Util.getIntSafe(idOfOrder);
	}

	public void setIdOfOrder(Integer idOfOrder) {
		this.idOfOrder = idOfOrder;
	}

    public void setDeliveryPizza(boolean deliveryPizza) {
        this.isDeliveryPizza = deliveryPizza;
    }

    public boolean isDeliveryPizza() {
        return isDeliveryPizza;
    }
}
