package at.ac.tuwien.sbc.valesriegler.group.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.*;

import at.ac.tuwien.sbc.valesriegler.group.GroupAgent;
import at.ac.tuwien.sbc.valesriegler.types.PizzaType;

/**
 * Panel which allows the selection of pizzas and the number of groups to create
 * <p />
 * Features a Create- and Cancel-Button which call the respective methods of the 
 * {@link GroupCreationHandler} set by {@link #setCreateAndCancelHandler(GroupCreationHandler)}
 * 
 * @author Gregor Riegler <gregor DOT riegler AT gmail DOT com>
 *
 */
@SuppressWarnings("serial")
public class GroupCreationDetailsPanel extends JPanel {
	
	private GroupCreationHandler handler;
	private int numberMembers;
	private JSpinner numberOfGroupsSpinner;
	private List<JComboBox> combos = new ArrayList<JComboBox>();
    private JTextField addressField = new JTextField();
    private final JComboBox pizzeriaDropdown = new JComboBox();

    public GroupCreationDetailsPanel(int numberMembers, boolean withAddressField) {
		this.numberMembers = numberMembers;
		
		GridLayout layout = new GridLayout(0, 2);
		setLayout(layout);
		
		for(int i=1; i<=numberMembers; i++) {
			JLabel memberLabel = new JLabel(i + ". Person");
			JComboBox pizzaDropdown = new JComboBox();
			combos.add(pizzaDropdown);
			for(PizzaType type : PizzaType.values()) {
				pizzaDropdown.addItem(type);
			}
			this.add(memberLabel);
			this.add(pizzaDropdown);
		}
		JLabel numberOfGroupsLabel = new JLabel("Number of groups to create: ");
		SpinnerNumberModel model = new SpinnerNumberModel(1,1,100,1);
	    numberOfGroupsSpinner = new JSpinner(model);
	    
	    this.add(numberOfGroupsLabel);
	    this.add(numberOfGroupsSpinner);

        final JLabel pizzeriaLabel = new JLabel("Pizzeria: ");

        final Set<String> pizzeriaIdentifiers = GroupAgent.getInstance().getPizzeriaIdentifiers();
        for (String pizzeriaIdentifier : pizzeriaIdentifiers) {
            pizzeriaDropdown.addItem(pizzeriaIdentifier);
        }

        this.add(pizzeriaLabel);
        this.add(pizzeriaDropdown);

        if(withAddressField) {
            final JLabel addressLabel = new JLabel("Address: ");
            this.add(addressLabel);
            this.add(addressField);
        }

        JButton create = new JButton("Create");
	    JButton cancel = new JButton("Cancel");
	    
	    this.add(create);
	    this.add(cancel);

	    addCreateHandler(create);
	    addCancelHandler(cancel);
	}


	private void addCancelHandler(JButton cancel) {
		cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				handler.handleCancel();
			}
		});
	}

	private void addCreateHandler(JButton create) {
		create.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				List<PizzaType> types = new ArrayList<PizzaType>();
				for(JComboBox combo : combos) {
					types.add((PizzaType) combo.getSelectedItem());
				}
				GroupCreationDetailsRequest gcr = new GroupCreationDetailsRequest(numberMembers, (int) numberOfGroupsSpinner.getValue(), types, addressField.getText(), (String) pizzeriaDropdown.getSelectedItem());
				handler.handleGroupCreation(gcr);
			}
		});
	}

	public void setCreateAndCancelHandler(
			GroupCreationHandler groupCreationCallback) {
		this.handler = groupCreationCallback;
	}

}
