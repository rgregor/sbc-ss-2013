package at.ac.tuwien.sbc.valesriegler.driver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.driver.jms.JMSDriver;

/**
 * DriverAgent parses the arguments and runs the JMS/XVSMDriver with the given ID.
 * 
 * @author jan
 * 
 */
public class DriverAgent {
	private static final String USAGE = "This application needs exactly 2 parameters: <\"XVSM\"|\"JMS\"> <XVSM-Space-Identifier|JMS-Server-URL> <ID of type Integer>";
	private static final Logger log = LoggerFactory.getLogger(DriverAgent.class);

	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
			throw new IllegalArgumentException(USAGE);
		}

		String mw = args[0];
		int parsedId = 0;
		try {
			parsedId = Integer.parseInt(args[2]);
		} catch (NumberFormatException e) {
			log.error(USAGE);
			return;
		}

		log.info("Middleware: " + mw + " ID:" + parsedId);
		if ("JMS".equalsIgnoreCase(mw)) {
			new JMSDriver(args[1], parsedId);
		} else if ("XVSM".equalsIgnoreCase(mw)) {
			// TODO: XVSM Driver?
		} else {
			throw new IllegalArgumentException(USAGE);
		}
	}

}
