package at.ac.tuwien.sbc.valesriegler.waiter.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.jms.nac.AbstractJMSNACMsgListener;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.AddressInfoRequest;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.AddressInfoResponse;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.BenchmarkStart;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.BenchmarkStop;
import at.ac.tuwien.sbc.valesriegler.waiter.WaiterAgent;
/**
 * Handles the NAC communication for the Group Agent.
 * 
 * @author jan
 * 
 */
public class WaiterJMSNACMsgListener extends AbstractJMSNACMsgListener {
	private static final Logger log = LoggerFactory.getLogger(WaiterJMSNACMsgListener.class);

	public WaiterJMSNACMsgListener() {
	}

	@Override
	public void onMessage(Message msg) {
		try {
			if (msg instanceof ObjectMessage) {
				ObjectMessage objMsg = (ObjectMessage) msg;
				Object obj = objMsg.getObject();
				if (obj instanceof AddressInfoRequest) {
					// DO NOTHING.
				} else if (obj instanceof AddressInfoResponse) {
					// DO NOTHING.
				} else if (obj instanceof BenchmarkStop) {
					WaiterAgent.jmsWaiter.getConnection().close();
				} else if (obj instanceof BenchmarkStart) {
					WaiterAgent.jmsWaiter.initJMS();

				} else {
					log.warn("Received unknown Object: " + obj);
				}
			} else {
				log.warn("Received unknown Message: " + msg);
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}
}
