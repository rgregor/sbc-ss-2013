package at.ac.tuwien.sbc.valesriegler.cook.actions;

import at.ac.tuwien.sbc.valesriegler.common.AbstractDeliveryAction;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;

import java.io.Serializable;

/**
 * response to the group's interest in pizza.
 * 
 * @author jan
 * 
 */
public class DeliveryOrderInfo extends AbstractDeliveryAction implements Serializable {
	private final int cookId;

	public DeliveryOrderInfo(DeliveryGroupData deliveryGroupData, int cookId) {
		super(deliveryGroupData);
		this.cookId = cookId;
	}

	public int getCookId() {
		return cookId;
	}

	@Override
	public String toString() {
		return "OrderInfo [cookId=" + cookId + "]";
	}

}
