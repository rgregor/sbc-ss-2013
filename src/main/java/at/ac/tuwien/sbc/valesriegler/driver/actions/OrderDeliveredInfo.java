package at.ac.tuwien.sbc.valesriegler.driver.actions;

import at.ac.tuwien.sbc.valesriegler.common.AbstractDeliveryAction;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;

import java.io.Serializable;

/**
 * response to the group's interest in pizza.
 * 
 * @author jan
 * 
 */
public class OrderDeliveredInfo extends AbstractDeliveryAction implements Serializable {
	private final int driverId;

	public OrderDeliveredInfo(DeliveryGroupData groupdata, int driverId) {
		super(groupdata);
		this.driverId = driverId;
	}

	public int getDriverId() {
		return driverId;
	}

	@Override
	public String toString() {
		return "OrderDeliveredInfo [driverId=" + driverId + ", getDeliveryGroupData()=" + getDeliveryGroupData() + "]";
	}
}
