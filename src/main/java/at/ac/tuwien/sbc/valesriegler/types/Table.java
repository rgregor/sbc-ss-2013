package at.ac.tuwien.sbc.valesriegler.types;

import at.ac.tuwien.sbc.valesriegler.common.HasId;
import org.mozartspaces.capi3.Queryable;

import java.io.Serializable;

/**
 * A table is a thing, where people can put things at. Possibly a laptop, to
 * have free hands to hold a pizzabox on the knees.
 * 
 * @author jan
 * 
 */
@Queryable(autoindex = true)
public class Table implements Serializable, HasId {
	private static int idNext = 0;
	private final Integer id;

	private Integer groupId = -1;

    /**
     * is needed for spaces linda selection
     */
    private Boolean isFree = true;

	public Table(Integer id) {
		this.id = id;
	}

	public Table() {
		id = ++idNext;
	}

	@Override
	public int getId() {
		return id;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
        if(groupId != -1) {
            isFree = false;
        } else {
            isFree = true;
        }
	}

    public void setFree(Boolean free) {
        isFree = free;
    }

	public boolean isFree() {
		if (groupId == -1)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "Table [id=" + id + ", groupId=" + groupId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Table other = (Table) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
