package at.ac.tuwien.sbc.valesriegler.types;

import java.io.Serializable;

/**
 * enum denoting all the kinds of pizzas we have. Thats pretty few :(
 * 
 * @author jan
 * 
 */
public enum PizzaType implements Serializable {
	MARGHERITA(5, 3), SALAMI(5.5, 7), CARDINALE(6, 5);

	public final double price;
	public final long duration;

	private PizzaType(double price, long duration) {
		this.price = price;
		this.duration = duration;
	}
}
