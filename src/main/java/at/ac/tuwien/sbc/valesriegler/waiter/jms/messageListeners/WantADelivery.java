package at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.group.actions.DeliveryOrderRequest;
import at.ac.tuwien.sbc.valesriegler.group.actions.DeliveryOrderResponse;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.JMSWaiter;

/**
 * Listener listening on the WantToOrder MQ, handling all incomming messages.
 * 
 * @author jan
 * 
 */
public class WantADelivery implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(WantADelivery.class);
	private final JMSWaiter waiter;

	public WantADelivery(JMSWaiter waiter) {
		this.waiter = waiter;
	}

	@Override
	public void onMessage(Message msg) {
		try {
			synchronized (waiter) {
				msg.acknowledge();
				if (msg instanceof ObjectMessage) {
					ObjectMessage objMsg = (ObjectMessage) msg;
					Object obj = objMsg.getObject();

					if (obj instanceof DeliveryOrderRequest) {
						DeliveryOrderRequest dor = (DeliveryOrderRequest) obj;
						log.debug("Received: " + dor);

						// generate random delay
						if (!Util.runSimulation) {
							Thread.sleep((long) (Math.random() * 10000));
						}

						ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(waiter.getCONNECTSTRING());
						Connection connection = connectionFactory.createConnection();
						connection.start();
						Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
						DeliveryOrderResponse or = new DeliveryOrderResponse(dor.getDeliveryGroupData(), waiter.getId());

						// Make cooks do their work
						MessageProducer prodOP = session.createProducer(session.createQueue("DeliveryOrdersToCook"));
						prodOP.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						// TODO: hack
						dor.getDeliveryGroupData().setWaiterIdOfOrder(waiter.getId());
						prodOP.send(session.createObjectMessage(dor));

						// inform Group + Pizzeria
						MessageProducer informGroup = session.createProducer(session.createQueue("GroupConnector"));
						informGroup.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						informGroup.send(session.createObjectMessage(or));

						MessageProducer informPizzeria = session.createProducer(session.createQueue("PizzeriaConnector"));
						informPizzeria.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						informPizzeria.send(session.createObjectMessage(or));

					} else {
						log.warn("Received unknown Object: " + obj);
					}
				} else {
					log.warn("Received unknown Message: " + msg);
				}
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);

		} catch (InterruptedException e) {
			log.error("EXCEPTION!", e);
		}
	}
}
