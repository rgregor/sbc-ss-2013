package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * response of paying.
 * 
 * @author jan
 * 
 */
public class PayResponse extends AbstractAction implements Serializable {
	private final int waiterId;

	public PayResponse(GroupData groupdata, int waiterId) {
		super(groupdata);
		this.waiterId = waiterId;
	}

	public int getWaiterId() {
		return waiterId;
	}

	@Override
	public String toString() {
		return "PayResponse [waiterId=" + waiterId + "]";
	}

}
