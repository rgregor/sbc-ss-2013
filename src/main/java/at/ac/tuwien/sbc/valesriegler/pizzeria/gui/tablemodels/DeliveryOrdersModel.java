package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;
import at.ac.tuwien.sbc.valesriegler.types.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeliveryOrdersModel extends AbstractOrdersModel<DeliveryGroupData> {
	private static final Logger log = LoggerFactory.getLogger(DeliveryOrdersModel.class);

	private static final String ID = "Delivery ID";
	private static final String ORDER_ID = "Order ID";
	private static final String DELIVERY_ADDRESS = "Address";
	private static final String STATUS = "Status";
	private static final String LOAD_BALANCER_ID = "Load Balancer";
	private static final String ORIGINAL_PIZZERIA = "From";
	private static final String TARGET_PIZZERIA = "To";

	private static final String[] COLUMNS = new String[]{ORDER_ID, ID, DELIVERY_ADDRESS, STATUS, LOAD_BALANCER_ID,
			ORIGINAL_PIZZERIA, TARGET_PIZZERIA};

	@Override
	protected String[] getColumns() {
		return COLUMNS;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<DeliveryGroupData> values = new ArrayList<DeliveryGroupData>(items.values());
		Collections.sort(values, Util.HAS_ORDER_COMPARATOR);
		DeliveryGroupData group = values.get(rowIndex);
		Order order = group.getOrder();
		String wantedColumn = COLUMNS[columnIndex];
		switch (wantedColumn) {
			case ID :
				return group.getId();
			case ORDER_ID :
				return order.getId();
			case DELIVERY_ADDRESS :
				return group.getAddress();
			case STATUS :
				return group.getDeliveryStatus();
			case LOAD_BALANCER_ID :
				return group.getLoadBalancerId();
			case ORIGINAL_PIZZERIA :
				return group.getOriginalPizzeriaId();
            case TARGET_PIZZERIA :
                return (group.getOriginalPizzeriaId() != null) ? group.getPizzeriaId() : "";
			default :
				throw new RuntimeException(UNHANDLEDCOLUMN);
		}
	}

	public DeliveryGroupData getGroupOfRow(int rowIndex) {
		List<DeliveryGroupData> values = new ArrayList<DeliveryGroupData>(items.values());
		Collections.sort(values, Util.HAS_ORDER_COMPARATOR);
		return values.get(rowIndex);
	}
}
