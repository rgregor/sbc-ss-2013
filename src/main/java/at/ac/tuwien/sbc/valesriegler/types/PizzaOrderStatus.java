package at.ac.tuwien.sbc.valesriegler.types;

import java.io.Serializable;

/**
 * Enum denoting all the possible states a PizzaOrder can be in.
 * 
 * @author jan
 * 
 */
public enum PizzaOrderStatus implements Serializable {
	NEW, ORDERED, IN_PREPARATION, DONE
}
