package at.ac.tuwien.sbc.valesriegler.group.gui;

import java.util.List;

import at.ac.tuwien.sbc.valesriegler.types.PizzaType;

public class GroupCreationDetailsRequest {
	public final int size;
	public final int numberOfGroups;
	public final List<PizzaType> pizzaTypes;
	public final String address;
	public final String pizzeria;

	public GroupCreationDetailsRequest(int size, int numberOfGroups, List<PizzaType> pizzaTypes, String address,
			String pizzeriaIdentifier) {
		this.size = size;
		this.numberOfGroups = numberOfGroups;
		this.pizzaTypes = pizzaTypes;
		this.address = address;
		pizzeria = pizzeriaIdentifier;
	}
}
