package at.ac.tuwien.sbc.valesriegler.common;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * Abstract action that can be performed in the DS. In almost all cases the
 * groupdata is relevant, it may be null in the few corner cases.
 * 
 * @author jan
 * 
 */
public abstract class AbstractAction implements Serializable {
	private final GroupData groupdata;

	public AbstractAction(GroupData groupdata) {
		this.groupdata = groupdata;
	}

	public GroupData getGroupdata() {
		return groupdata;
	}
}