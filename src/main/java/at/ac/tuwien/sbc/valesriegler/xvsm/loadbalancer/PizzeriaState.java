package at.ac.tuwien.sbc.valesriegler.xvsm.loadbalancer;


public class PizzeriaState {

    private final int id;
    private long lastUpdateTime = 0;
    private int numberDeliveries = 0;
    private PizzeriaStatus status = PizzeriaStatus.ONLINE;

    public PizzeriaState(int id) {
        this.id = id;
    }

    public PizzeriaStatus getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public int getNumberDeliveries() {
        return numberDeliveries;
    }

    public void setNumberDeliveries(int numberDeliveries) {
        this.numberDeliveries = numberDeliveries;
    }

    public void setStatus(PizzeriaStatus status) {
        this.status = status;
    }
}
