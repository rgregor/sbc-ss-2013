package at.ac.tuwien.sbc.valesriegler.xvsm.spacehelpers;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.notifications.NotificationManager;
import org.mozartspaces.notifications.Operation;

public class SpaceListenerImplBuilder {
    private Capi capi;
    private ContainerReference cref;
    private int timeout = 3000;
    private SpaceAction spaceAction;
    private boolean lookaround = false;
    private NotificationManager notificationManager;
    private boolean countAll = false;
    private String name;
    private boolean noNotification = false;

    public SpaceListenerImplBuilder setCapi(Capi capi) {
        this.capi = capi;
        return this;
    }

    public SpaceListenerImplBuilder setCref(ContainerReference cref) {
        this.cref = cref;
        return this;
    }

    public SpaceListenerImplBuilder setTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public SpaceListenerImplBuilder setSpaceAction(SpaceAction spaceAction) {
        this.spaceAction = spaceAction;
        return this;
    }

    public SpaceListenerImplBuilder setNotificationManager(NotificationManager mgr) {
        this.notificationManager = mgr;
        return this;
    }

    public SpaceListenerImplBuilder enableCountAll() {
        this.countAll = true;
        return this;
    }

    public SpaceListenerImplBuilder setLookaround(boolean lookaround) {
        this.lookaround = lookaround;
        return this;
    }

    public SpaceListenerImpl createSpaceListenerImpl() {
        final SpaceListenerImpl spaceListener = new SpaceListenerImpl(capi, cref, timeout, spaceAction, notificationManager, lookaround, countAll, name, noNotification);
        spaceListener.startHandlingAbsenceOfNotifications();
        try {
           if(! noNotification) {
               notificationManager.createNotification(cref, spaceListener, Operation.WRITE);
           }
        } catch (Exception e) {
            Util.handleSpaceErrorAndTerminate(e);
        }
        return spaceListener;
    }

    public SpaceListenerImplBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SpaceListenerImplBuilder noNotification() {
        this.noNotification = true;
        return this;
    }
}