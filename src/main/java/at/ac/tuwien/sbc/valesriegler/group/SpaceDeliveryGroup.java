package at.ac.tuwien.sbc.valesriegler.group;


import at.ac.tuwien.sbc.valesriegler.xvsm.DeliveryGroupXVSM;

public class SpaceDeliveryGroup  implements Runnable {
    private DeliveryGroupXVSM xvsm;

    public SpaceDeliveryGroup(int groupId, int pizzeriaSpacePort, String address) {
        xvsm = new DeliveryGroupXVSM(groupId, pizzeriaSpacePort, address);
    }

    @Override
    public void run() {
        xvsm.waitForMyOrder();
    }

}
