package at.ac.tuwien.sbc.valesriegler.jms.nac.actions;

/**
 * Message hat instructs everyone to reply their Address.
 * 
 * @author jan
 * 
 */
public class BenchmarkStart extends AbstractNACMsg {
	public BenchmarkStart() {
	}

	@Override
	public String toString() {
		return "BenchmarkStop []";
	}
}
