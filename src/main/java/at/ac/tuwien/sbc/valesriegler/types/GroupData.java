package at.ac.tuwien.sbc.valesriegler.types;

import java.io.Serializable;

import org.mozartspaces.capi3.Queryable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;

/**
 * This class represents all of a group's data, that can be relevant to send
 * across the net.
 * 
 * @author jan
 * 
 */
@Queryable(autoindex = true)
public class GroupData implements Serializable, HasOrder {
	private static final Logger log = LoggerFactory.getLogger(GroupData.class);

	private Integer id;
	private GroupState state = GroupState.WAITING;
	private Integer size;

	private Table table;
	private Integer tableWaiter;

	private Order order;
	private Integer orderWaiter;
	private Integer servingWaiter;
	private Integer payingWaiter;
	private String pizzeriaId;

	public GroupData() {
	}

	public GroupData(Integer id) {
		this.id = id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int getId() {
		return Util.getIntSafe(id);
	}

	@Override
	public Order getOrder() {
		return order;
	}

	public int getOrderWaiter() {
		return Util.getIntSafe(orderWaiter);
	}

	public int getPayingWaiter() {
		return Util.getIntSafe(payingWaiter);
	}

	public int getServingWaiter() {
		return Util.getIntSafe(servingWaiter);
	}

	public String getPizzeriaId() {
		return pizzeriaId;
	}

	public void setPizzeriaId(String pizzeriaId) {
		this.pizzeriaId = pizzeriaId;
	}

	public int getSize() {
		return Util.getIntSafe(size);
	}

	public GroupState getState() {
		return state;
	}

	public Table getTable() {
		return table;
	}

	public int getTableWaiter() {
		return Util.getIntSafe(tableWaiter);
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void setOrderWaiter(int orderWaiter) {
		this.orderWaiter = orderWaiter;
	}

	public void setPayingWaiter(int payingWaiter) {
		this.payingWaiter = payingWaiter;
	}

	public void setServingWaiter(int servingWaiter) {
		this.servingWaiter = servingWaiter;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setState(GroupState state) {
		this.state = state;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public void setTableWaiter(int tableWaiter) {
		this.tableWaiter = tableWaiter;
	}

	@Override
	public String toString() {
		return "GroupData{" + "id=" + id + ", state=" + state + ", size=" + size + ", table=" + table + ", tableWaiter="
				+ tableWaiter + ", order=" + order + ", orderWaiter=" + orderWaiter + ", servingWaiter=" + servingWaiter
				+ ", payingWaiter=" + payingWaiter + '}';
	}
}
