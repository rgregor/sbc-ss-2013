package at.ac.tuwien.sbc.valesriegler.group.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

public class GroupCreationPanel extends JPanel {

	private boolean createDeliveryGroups;

	public GroupCreationPanel(final boolean createDeliveryGroups) {
		this.createDeliveryGroups = createDeliveryGroups;

		final JPanel chooseGroupSizePanel = new JPanel();
		JLabel creationLabel = new JLabel("How many members should the group have?");
		JButton next = new JButton("Next");
		SpinnerNumberModel model = new SpinnerNumberModel(1, 1, 4, 1);
		final JSpinner spinner = new JSpinner(model);

		// When 'next' is clicked the second, final panel of the Group Creation
		// Wizard should be shown
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int numberMembers = (int) spinner.getValue();
				GroupCreationPanel.this.createDeliveryGroups = createDeliveryGroups;
				final GroupCreationDetailsPanel groupCreationDetailsPanel = new GroupCreationDetailsPanel(numberMembers,
						GroupCreationPanel.this.createDeliveryGroups);
				GroupCreationHandler groupCreationHandler = new GroupCreationHandler(GroupCreationPanel.this,
						chooseGroupSizePanel, groupCreationDetailsPanel, GroupCreationPanel.this.createDeliveryGroups);

				groupCreationDetailsPanel.setCreateAndCancelHandler(groupCreationHandler);
				groupCreationHandler.showGroupCreationDetailPanel();

			}
		});

		GridLayout creationPanelLayout = new GridLayout(3, 1);
		chooseGroupSizePanel.setLayout(creationPanelLayout);
		final String title = createDeliveryGroups ? "Create Delivery Groups" : "Create Normal Groups";
		setBorder(new TitledBorder(title));

		this.add(chooseGroupSizePanel);
		chooseGroupSizePanel.add(creationLabel);
		chooseGroupSizePanel.add(spinner);
		chooseGroupSizePanel.add(next);

	}
}
