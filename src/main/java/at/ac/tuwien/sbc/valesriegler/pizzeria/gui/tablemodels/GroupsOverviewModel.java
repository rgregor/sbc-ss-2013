package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;

import at.ac.tuwien.sbc.valesriegler.common.TableModel;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;
import at.ac.tuwien.sbc.valesriegler.types.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class GroupsOverviewModel extends TableModel<GroupData> {
    private static final Logger log = LoggerFactory.getLogger(GroupsOverviewModel.class);
	private static final String ID = "ID";
	private static final String SIZE = "Size";

	private static final String[] COLUMNS = new String[] { ID, SIZE };

	@Override
	protected String[] getColumns() {
		return COLUMNS;
	}
	

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<GroupData> values = new ArrayList<GroupData>(items.values());
		GroupData group = values.get(rowIndex);
		String wantedColumn = COLUMNS[columnIndex];
		switch (wantedColumn) {
		case ID:
			return group.getId();
		case SIZE:
			return group.getSize();
		default:
			throw new RuntimeException(UNHANDLEDCOLUMN);
		}
	}

	public void removeGroup(int id) {
		items.remove(id);
		
		fireTableDataChanged();
	}

    public void removeGroupsFromTables(List<Table> tables) {
        synchronized (items) {
            for (Table table : tables) {
                final int groupId = table.getGroupId();
                items.remove(groupId);
            }
        }
        fireTableDataChanged();
    }
}
