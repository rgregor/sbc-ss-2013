package at.ac.tuwien.sbc.valesriegler.common;

public interface HasId {
	public int getId();
}
