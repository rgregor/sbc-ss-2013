package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * response to the group's interest in pizza.
 * 
 * @author jan
 * 
 */
public class OrderResponse extends AbstractAction implements Serializable {
	private final int waiterId;

	public OrderResponse(GroupData groupdata, int waiterId) {
		super(groupdata);
		this.waiterId = waiterId;
	}

	public int getWaiterId() {
		return waiterId;
	}

	@Override
	public String toString() {
		return "OrderResponse [waiterId=" + waiterId + ", getGroupdata()=" + getGroupdata() + "]";
	}

}
