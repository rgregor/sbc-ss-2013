package at.ac.tuwien.sbc.valesriegler.balancer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BalancerAgent parses the arguments and runs the JMS/XVSMBalancer balancing all pizzerias.
 * 
 * @author jan
 * 
 */
public class BalancerAgent {
	private static final String USAGE = "This application needs exactly 2 parameters: <\"XVSM\"|\"JMS\"> <XVSM-Space-Identifier|JMS-Naming-Service-URL>";
	private static final Logger log = LoggerFactory.getLogger(BalancerAgent.class);

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			throw new IllegalArgumentException(USAGE);
		}

		String mw = args[0];
		int parsedId = 0;
		try {
			parsedId = Integer.parseInt(args[2]);
		} catch (NumberFormatException e) {
			log.error(USAGE);
			return;
		}

		log.info("Middleware: " + mw + " ID:" + parsedId);
		if ("JMS".equalsIgnoreCase(mw)) {
			// TODO: balancer
		} else if ("XVSM".equalsIgnoreCase(mw)) {
			// TODO: XVSM Balancer??
		} else {
			throw new IllegalArgumentException(USAGE);
		}
	}

}
