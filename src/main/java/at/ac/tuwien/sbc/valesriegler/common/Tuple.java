package at.ac.tuwien.sbc.valesriegler.common;


public class Tuple<T> {
    public final T snd;
    public final T fst;

    public Tuple(T fst, T snd) {
        this.fst = fst;
        this.snd = snd;
    }
}
