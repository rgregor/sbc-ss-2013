package at.ac.tuwien.sbc.valesriegler.group.gui;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.group.DeliveryGroup;
import at.ac.tuwien.sbc.valesriegler.group.Group;
import at.ac.tuwien.sbc.valesriegler.group.GroupAgent;
import at.ac.tuwien.sbc.valesriegler.types.Order;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the Wizard-like creation of groups.
 * <p />
 * Give an instance of this class as a callback to the Panel from which a
 * group creation request should be issued
 */
class GroupCreationHandler {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GroupCreationHandler.class);
    private final boolean createDeliveryGroups;
    private JPanel creationPanel;
    private JPanel chooseGroupSizePanel;
    private JPanel groupCreationPanel;

    public GroupCreationHandler(JPanel creationPanel, JPanel chooseGroupSizePanel, JPanel groupCreationPanel, boolean createDeliveryGroups) {
        this.createDeliveryGroups = createDeliveryGroups;
        this.creationPanel = creationPanel;
        this.chooseGroupSizePanel = chooseGroupSizePanel;
        this.groupCreationPanel = groupCreationPanel;
    }

    public void showGroupCreationDetailPanel() {
        chooseGroupSizePanel.setVisible(false);
        creationPanel.add(groupCreationPanel);
    }

    public void handleGroupCreation(GroupCreationDetailsRequest gc) {
        chooseGroupSizePanel.setVisible(true);
        creationPanel.remove(groupCreationPanel);
        creationPanel.repaint();
        createGroups(gc);
    }

    public void createGroups(GroupCreationDetailsRequest gc) {
        List<DeliveryGroup> newDeliveryGroups = new ArrayList<DeliveryGroup>();
        List<Group> newNormalGroups = new ArrayList<Group>();

        for (int i = 0; i < gc.numberOfGroups; i++) {
            if(createDeliveryGroups) {
                DeliveryGroup group = new DeliveryGroup();
                final Order order = Util.createOrder(group, gc, true);
                group.getDeliveryGroupData().setOrder(order);
                group.getDeliveryGroupData().setPizzeriaId(gc.pizzeria);
                group.getDeliveryGroupData().setAddress(gc.address);
                newDeliveryGroups.add(group);
            } else {
                Group group = new Group();
                final Order order = Util.createOrder(group, gc, false);
                group.getGroupData().setOrder(order);
                group.getGroupData().setSize(gc.size);
                group.getGroupData().setPizzeriaId(gc.pizzeria);
                newNormalGroups.add(group);
            }
        }
        if(createDeliveryGroups) {
            DeliveryOverviewModel deliveryOverviewModel = GroupAgent.getInstance().getDeliveryModel();
            deliveryOverviewModel.addItems(newDeliveryGroups);
            GroupAgent.getInstance().onDeliveryGroupsCreated(newDeliveryGroups);
        } else {
            GroupOverviewModel groupModel = GroupAgent.getInstance().getGroupModel();
            groupModel.addItems(newNormalGroups);
            GroupAgent.getInstance().onGroupsCreated(newNormalGroups);
        }
    }

    public void handleCancel() {
        chooseGroupSizePanel.setVisible(true);
        creationPanel.remove(groupCreationPanel);
        creationPanel.repaint();
    }
}
