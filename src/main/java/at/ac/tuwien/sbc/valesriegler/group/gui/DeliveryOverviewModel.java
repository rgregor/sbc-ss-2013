package at.ac.tuwien.sbc.valesriegler.group.gui;

import at.ac.tuwien.sbc.valesriegler.common.TableModel;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.group.DeliveryGroup;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DeliveryOverviewModel extends TableModel<DeliveryGroup> {
	private static final Logger log = LoggerFactory.getLogger(GroupOverviewModel.class);

	private static final String STATE = "State";
	private static final String PIZZAS = "Pizzas";
	private static final String ADDRESS = "Address";
	private static final String ID = "ID";
	private static final String PIZZERIA = "Pizzeria";
	private static final String[] COLUMNS = new String[]{ID, ADDRESS, PIZZAS, STATE, PIZZERIA};

	@Override
	protected String[] getColumns() {
		return COLUMNS;
	}

	@Override
	public void addItems(List<DeliveryGroup> newItems) {
		log.info("addItems()");
		super.addItems(newItems);
		for (DeliveryGroup g : newItems) {
			if (Util.useJMS)
				g.orderSomeFood();
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<DeliveryGroup> values = new ArrayList<DeliveryGroup>(items.values());
		DeliveryGroup group = values.get(rowIndex);
		String wantedColumn = COLUMNS[columnIndex];
		switch (wantedColumn) {
			case ID :
				int groupId = group.getId();
				return groupId;
			case ADDRESS :
				return group.getDeliveryGroupData().getAddress();
			case PIZZAS :
				if (group.getDeliveryGroupData().getOrder() == null)
					return "none";
				return Util.pizzaDisplay(group.getDeliveryGroupData().getOrder().getOrderedPizzas());
			case STATE :
				return group.getDeliveryGroupData().getDeliveryStatus();
			case PIZZERIA :
				return group.getDeliveryGroupData().getPizzeriaId();
			default :
				throw new RuntimeException(UNHANDLEDCOLUMN);
		}
	}

	public void addDeliveries(List<DeliveryGroupData> groups) {
		synchronized (items) {
			for (DeliveryGroupData group : groups) {
                if(group.getDeliveryStatus() == DeliveryStatus.MOVED) {
                    continue;
                }
				final DeliveryGroup deliveryGroup = items.get(group.getId());
				if (deliveryGroup == null) {
					log.error("Delivery group not found: {}", group.getId());
				} else {
					deliveryGroup.setDeliveryGroupData(group);
                }
			}
		}
		fireTableDataChanged();
	}

	public void createStatistics() {
		int size;
		int finished;
		synchronized (items) {
			final Collection<DeliveryGroup> values = items.values();
			size = values.size();
			finished = 0;
			for (DeliveryGroup group : values) {
				final DeliveryStatus status = group.getDeliveryGroupData().getDeliveryStatus();
				if (status == DeliveryStatus.DELIVERED || status == DeliveryStatus.DELIVERY_FAILED) {
					finished++;
				}
			}
		}
		log.warn("{} deliveries were ordered", size);
		log.warn("{} were finished", finished);
	}

	public boolean hasFinished() {
		int size;
		int finished;
		synchronized (items) {
			final Collection<DeliveryGroup> values = items.values();
			size = values.size();
			finished = 0;
			for (DeliveryGroup group : values) {
				final DeliveryStatus status = group.getDeliveryGroupData().getDeliveryStatus();
				if (status == DeliveryStatus.DELIVERED || status == DeliveryStatus.DELIVERY_FAILED) {
					finished++;
				}
			}
		}
		final boolean result = size == finished;
		return result;
	}
}
