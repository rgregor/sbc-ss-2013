package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * indicate interest to get a table to sitdown.
 * 
 * @author jan
 * 
 */
public class TableRequest extends AbstractAction implements Serializable {
	public TableRequest(GroupData groupdata) {
		super(groupdata);
	}

	@Override
	public String toString() {
		return "TableRequest []";
	}

}
