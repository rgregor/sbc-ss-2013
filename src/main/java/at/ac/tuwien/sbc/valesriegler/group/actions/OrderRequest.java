package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * Indicate the interest to order pizzas
 * 
 * @author jan
 * 
 */
public class OrderRequest extends AbstractAction implements Serializable {
	public OrderRequest(GroupData groupdata) {
		super(groupdata);
	}

	@Override
	public String toString() {
		return "OrderRequest [getGroupdata()=" + getGroupdata() + "]";
	}

}
