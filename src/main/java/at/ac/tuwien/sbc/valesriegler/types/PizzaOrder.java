package at.ac.tuwien.sbc.valesriegler.types;

import at.ac.tuwien.sbc.valesriegler.common.HasId;
import at.ac.tuwien.sbc.valesriegler.common.Util;

import java.io.Serializable;

/**
 * Class denoting one Pizza in the future and its state.
 * 
 * @author jan
 * 
 */
public class PizzaOrder implements Serializable, HasId, Comparable<PizzaOrder> {
	private static int nextID = 0;
	protected Integer id;

	protected Integer orderId;
	protected PizzaType pizzaType;
	protected PizzaOrderStatus status;
	protected Integer cookId;
    protected Boolean isDeliveryPizza;

    public PizzaOrder(PizzaType pizzaType) {
		id = ++nextID;
		this.pizzaType = pizzaType;
		status = PizzaOrderStatus.NEW;
	}

	public PizzaOrder(int id) {
		this.id = id;
	}

	public PizzaOrder() {
	}

    public PizzaType getPizzaType() {
		return pizzaType;
	}

    public boolean getDeliveryPizza() {
        return isDeliveryPizza;
    }

	public PizzaOrderStatus getStatus() {
		return status;
	}

    public void setDeliveryPizza(Boolean deliveryPizza) {
        isDeliveryPizza = deliveryPizza;
    }

	public int getId() {
		return Util.getIntSafe(id);
	}

	public int getCookId() {
		return Util.getIntSafe(cookId);
	}

	public void setCookId(int cookId) {
		this.cookId = cookId;
	}

	public void setStatus(PizzaOrderStatus status) {
		this.status = status;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPizzaType(PizzaType pizzaType) {
		this.pizzaType = pizzaType;
	}



	public int getOrderId() {
		return Util.getIntSafe(orderId);
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "PizzaOrder [id=" + id + ", orderId=" + orderId + ", pizzaType="
				+ pizzaType + ", status=" + status + "]";
	}

    @Override
    public int compareTo(PizzaOrder o) {
        final int id2 = o.orderId;
        if (orderId < id2) return -1;
        else if (orderId > id2) return 1;
        return 0;
    }
}
