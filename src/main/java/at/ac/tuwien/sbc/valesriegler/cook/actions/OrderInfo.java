package at.ac.tuwien.sbc.valesriegler.cook.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * response to the group's interest in pizza.
 * 
 * @author jan
 * 
 */
public class OrderInfo extends AbstractAction implements Serializable {
	private final int cookId;

	public OrderInfo(GroupData groupdata, int cookId) {
		super(groupdata);
		this.cookId = cookId;
	}

	public int getCookId() {
		return cookId;
	}

	@Override
	public String toString() {
		return "OrderInfo [cookId=" + cookId + "]";
	}

}
