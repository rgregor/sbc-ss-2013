package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * Leave table.
 * 
 * @author jan
 * 
 */
public class TableFree extends AbstractAction implements Serializable {
	public TableFree(GroupData groupdata) {
		super(groupdata);
	}

	@Override
	public String toString() {
		return "TableFree []";
	}

}
