package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;

import at.ac.tuwien.sbc.valesriegler.common.TableModel;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;


public class WaitersOfOrderModel extends TableModel<GroupData> {
	private static final String TABLE_ASSIGNMENT = "Table Assignment";
	private static final String ORDER = "Order";
	private static final String SERVING = "Serving";
	private static final String PAYMENT = "Payment";
	private static final String[] COLUMNS = new String[] { TABLE_ASSIGNMENT, ORDER, SERVING, PAYMENT };

	protected GroupData groupData;

	public void setCurrentGroup(GroupData groupData) {
		this.groupData = groupData;

		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return groupData == null ? 0 : 1;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex > 0)
			return null;

		String wantedColumn = COLUMNS[columnIndex];
		switch (wantedColumn) {
		case TABLE_ASSIGNMENT:
			return Util.getId(groupData.getTableWaiter());
		case ORDER:
			return Util.getId(groupData.getOrderWaiter());
		case SERVING:
			return Util.getId(groupData.getServingWaiter());
		case PAYMENT:
			return Util.getId(groupData.getPayingWaiter());
		default:
			throw new RuntimeException(UNHANDLEDCOLUMN);
		}
	}

	@Override
	protected String[] getColumns() {
		return COLUMNS;
	}

}
