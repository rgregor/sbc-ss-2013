package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;
import at.ac.tuwien.sbc.valesriegler.types.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdersOverviewModel extends AbstractOrdersModel<GroupData> {
    private static final Logger log = LoggerFactory.getLogger(OrdersOverviewModel.class);

	private static final String ID = "Order ID";
	private static final String TABLE_ID = "Table ID";
	private static final String GROUP_ID = "Group ID";
	private static final String STATUS = "Status";

	private static final String[] COLUMNS = new String[] { ID, TABLE_ID, GROUP_ID, STATUS };

    @Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		List<GroupData> values = new ArrayList<GroupData>(items.values());
        Collections.sort(values, Util.HAS_ORDER_COMPARATOR);
        GroupData group = values.get(rowIndex);
		Order order = group.getOrder();
		String wantedColumn = COLUMNS[columnIndex];
		switch (wantedColumn) {
		case ID:
			return String.format(Util.NUMBER_DISPLAY_FORMAT, order.getId());
		case TABLE_ID:
			return String.format(Util.NUMBER_DISPLAY_FORMAT, group.getTable().getId());
		case GROUP_ID:
			return String.format(Util.NUMBER_DISPLAY_FORMAT, order.getGroupId());
		case STATUS:
			return order.getStatus();
		default:
			throw new RuntimeException(UNHANDLEDCOLUMN);
		}
	}

	public GroupData getGroupOfRow(int rowIndex) {
        final List<GroupData> values = new ArrayList<GroupData>(items.values());
        Collections.sort(values, Util.HAS_ORDER_COMPARATOR);
        return new ArrayList<GroupData>(values).get(rowIndex);
	}

	@Override
	protected String[] getColumns() {
		return COLUMNS;
	}

	public void updateGroupData(GroupData groupdata) {
		synchronized (items) {
			items.put(groupdata.getId(), groupdata);
		}

		fireTableDataChanged();
	}
}
