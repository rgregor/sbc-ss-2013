package at.ac.tuwien.sbc.valesriegler.xvsm.loadbalancer;

public enum PizzeriaStatus {
    ONLINE, OFFLINE;
}
