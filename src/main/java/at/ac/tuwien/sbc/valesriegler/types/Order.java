package at.ac.tuwien.sbc.valesriegler.types;

import at.ac.tuwien.sbc.valesriegler.common.HasId;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import org.mozartspaces.capi3.Queryable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Object denotes a Group's interest in pizzas and the state of the pizzas
 * themselves.
 * 
 * @author jan
 * 
 */
@Queryable(autoindex = true)
public class Order implements Serializable, HasId {
	private static int idNext = 0;
	private Integer id;
	private Integer groupId;
	private OrderStatus status;

	private List<PizzaOrder> orderedPizzas;
	private List<Pizza> cookedPizzas;

	// this is necessary so that i can make a xvsm linda selection on it
	private Integer numberOfPizzas;

	public Order() {
	}

	public Order(int groupId, List<PizzaOrder> orderedPizzas) {
		// TODO don't set the id here but let the waiter set it
		// genId();
		this.groupId = groupId;
		status = OrderStatus.NEW;
		this.orderedPizzas = orderedPizzas;
		cookedPizzas = new ArrayList<Pizza>();
	}

	public Order(HasId group, List<PizzaOrder> orderedPizzas) {
		// TODO don't set the id here but let the waiter set it
		// genId();
		groupId = group.getId();
		status = OrderStatus.NEW;
		this.orderedPizzas = orderedPizzas;
		cookedPizzas = new ArrayList<Pizza>();
	}

	public void addCookedPizza(Pizza pizza) {
		cookedPizzas.add(pizza);
	}

	public List<Pizza> getCookedPizzas() {
		return cookedPizzas;
	}

	public int getGroupId() {
		return groupId;
	}

	public int getNumberOfPizzas() {
		return Util.getIntSafe(numberOfPizzas);
	}

	public void setNumberOfPizzas(Integer numberOfPizzas) {
		this.numberOfPizzas = numberOfPizzas;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void genId() {
		id = ++idNext;
	}

	@Override
	public int getId() {
		return Util.getIntSafe(id);
	}

	public List<PizzaOrder> getOrderedPizzas() {
		return orderedPizzas;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setCookedPizzas(List<Pizza> cookedPizzas) {
		this.cookedPizzas = cookedPizzas;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public void setOrderedPizzas(List<PizzaOrder> orderedPizzas) {
		this.orderedPizzas = orderedPizzas;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", groupId=" + groupId + ", status=" + status + ", orderedPizzas=" + orderedPizzas
				+ ", cookedPizzas=" + cookedPizzas + "]";
	}
}
