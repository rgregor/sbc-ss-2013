package at.ac.tuwien.sbc.valesriegler.types;

import at.ac.tuwien.sbc.valesriegler.common.HasId;

public interface HasOrder extends HasId {
    Order getOrder();
}
