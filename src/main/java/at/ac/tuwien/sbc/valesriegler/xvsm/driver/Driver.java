package at.ac.tuwien.sbc.valesriegler.xvsm.driver;

import at.ac.tuwien.sbc.valesriegler.common.Tuple;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.xvsm.DriverXVSM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Driver {
    private static final String USAGE = "Driver needs exactly two integer parameters: DRIVERID, PIZZERIA-SPACE-PORT";
    private static final Logger log = LoggerFactory.getLogger(Driver.class);

    private final int port;
    private final int id;
    private DriverXVSM xvsm;

    public static void main(String[] args) {
        final Tuple<Integer> driverIdAndSpacePort = Util.parseIdAndSpacePort(args, USAGE);

        Driver driver = new Driver(driverIdAndSpacePort.fst, driverIdAndSpacePort.snd);
        driver.start();
    }


    private void start() {
        xvsm = new DriverXVSM(id, port);
        xvsm.listenForPreparedDeliveryOrders();
        xvsm.listenForPreparedDeliveryPizzas();
    }

    public Driver(int id, int port) {
        this.id = id;
        this.port = port;
        log.info("I AM A Driver WITH ID {}", id);
    }

}
