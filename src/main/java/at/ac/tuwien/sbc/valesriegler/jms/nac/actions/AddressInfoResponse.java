package at.ac.tuwien.sbc.valesriegler.jms.nac.actions;


/**
 * Response to the group's interest in pizza.
 * 
 * @author jan
 * 
 */
public class AddressInfoResponse extends AbstractNACMsg {
	final private String address;

	public AddressInfoResponse(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "AddressInfoResponse [address=" + address + "]";
	}

}
