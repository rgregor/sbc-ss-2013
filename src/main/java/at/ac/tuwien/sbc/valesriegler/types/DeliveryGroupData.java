package at.ac.tuwien.sbc.valesriegler.types;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import org.mozartspaces.capi3.Queryable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Queryable(autoindex = true)
public class DeliveryGroupData implements Serializable, HasOrder {
	private static final Logger log = LoggerFactory.getLogger(GroupData.class);

	private Integer id;
	private Order order;
	private String address;
	private DeliveryStatus deliveryStatus = DeliveryStatus.START;
	private String pizzeriaId;

	private Integer waiterIdOfOrder;
	private Integer driverId;

	/**
	 * These fields are set only when the delivery order was shifted by a load balancer from one pizzeria to another
	 */
	private Integer loadBalancerId;
	private String originalPizzeriaId;

	public DeliveryGroupData(int id) {
		this.id = id;
	}

	public DeliveryGroupData() {
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getAddress() {
		return address;
	}

	public String getOriginalPizzeriaId() {
		return originalPizzeriaId;
	}

	public int getDriverId() {
		return Util.getIntSafe(driverId);
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public int getWaiterIdOfOrder() {
		return Util.getIntSafe(waiterIdOfOrder);
	}

	public void setWaiterIdOfOrder(Integer waiterIdOfOrder) {
		this.waiterIdOfOrder = waiterIdOfOrder;
	}

	public void setOriginalPizzeriaId(String originalPizzeriaId) {
		this.originalPizzeriaId = originalPizzeriaId;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public DeliveryStatus getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public int getLoadBalancerId() {
		return Util.getIntSafe(loadBalancerId);
	}

	public void setLoadBalancerId(Integer loadBalancerId) {
		this.loadBalancerId = loadBalancerId;
	}

	public String getPizzeriaId() {
		return pizzeriaId;
	}

	public void setPizzeriaId(String pizzeriaId) {
		this.pizzeriaId = pizzeriaId;
	}

	@Override
	public int getId() {
		return Util.getIntSafe(id);
	}

	@Override
	public String toString() {
		return "DeliveryGroupData [id=" + id + ", order=" + order + ", address=" + address + ", deliveryStatus="
				+ deliveryStatus + ", pizzeriaId=" + pizzeriaId + ", waiterIdOfOrder=" + waiterIdOfOrder + ", driverId="
				+ driverId + ", loadBalancerId=" + loadBalancerId + ", originalPizzeriaId=" + originalPizzeriaId + "]";
	}
}
