package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractDeliveryAction;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;

/**
 * This is the phonecall to a pizzeria.
 * 
 * @author jan
 * 
 */
public class DeliveryOrderRequest extends AbstractDeliveryAction implements Serializable {
	public DeliveryOrderRequest(DeliveryGroupData deliveryGroupData) {
		super(deliveryGroupData);
	}

	@Override
	public String toString() {
		return "DeliveryOrderRequest [getDeliveryGroupData()=" + getDeliveryGroupData() + "]";
	}

}
