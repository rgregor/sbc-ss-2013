package at.ac.tuwien.sbc.valesriegler.group.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.group.GroupAgent;
import at.ac.tuwien.sbc.valesriegler.jms.nac.AbstractJMSNACMsgListener;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.AddressInfoRequest;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.AddressInfoResponse;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.BenchmarkStart;
import at.ac.tuwien.sbc.valesriegler.jms.nac.actions.BenchmarkStop;
/**
 * Handles the NAC communication for the Group Agent.
 * 
 * @author jan
 * 
 */
public class GroupJMSNACMsgListener extends AbstractJMSNACMsgListener implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(GroupJMSNACMsgListener.class);

	public GroupJMSNACMsgListener() {
		Thread th = new Thread(this);
		th.setDaemon(true);
		th.start();
	}

	@Override
	public void onMessage(Message msg) {
		try {
			if (msg instanceof ObjectMessage) {
				ObjectMessage objMsg = (ObjectMessage) msg;
				Object obj = objMsg.getObject();
				if (obj instanceof AddressInfoRequest) {
					log.debug("Received: " + obj);
					return;
					// DO NOTHING.

				} else if (obj instanceof AddressInfoResponse) {
					log.debug("Received: " + obj);
					AddressInfoResponse response = (AddressInfoResponse) obj;
					GroupAgent.getInstance().getPizzeriaIdentifiers().add(response.getAddress());
					if (!Util.runSimulation) {
						GroupAgent.getGroupGui().getFrame().repaint();
						JMSGroupConnector.getConnectors().put(response.getAddress(), new JMSGroupConnector(response.getAddress()));
					}
					return;

				} else if (obj instanceof BenchmarkStop) {
					log.warn("Received: " + obj);
					for (String keys : JMSGroupConnector.getConnectors().keySet()) {
						JMSGroupConnector c = JMSGroupConnector.getConnector(keys);
						c.getConnection().close();
						GroupAgent.getInstance().getDeliveryModel().createStatistics();
						GroupAgent.getJmsnac().stop();
					}
					return;

				} else if (obj instanceof BenchmarkStart) {
					log.debug("Received: " + obj);
					return;
					// DO NOTHING.

				} else {
					log.warn("Received unknown Object: " + obj);
				}
			} else {
				log.warn("Received unknown Message: " + msg);
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}
	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(15000);
				GroupAgent.getInstance().getPizzeriaIdentifiers().clear();
				getJmsnac().sendNACMsg(new AddressInfoRequest());
			} catch (Exception e) {
				log.warn("EXCEPTION!", e);
			}
		}
	}

}
