package at.ac.tuwien.sbc.valesriegler.pizzeria.jms;

import java.util.ArrayList;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.cook.actions.DeliveryOrderInfo;
import at.ac.tuwien.sbc.valesriegler.cook.actions.OrderInfo;
import at.ac.tuwien.sbc.valesriegler.driver.actions.OrderDeliveredInfo;
import at.ac.tuwien.sbc.valesriegler.group.actions.DeliveryOrderResponse;
import at.ac.tuwien.sbc.valesriegler.group.actions.OrderResponse;
import at.ac.tuwien.sbc.valesriegler.group.actions.PayResponse;
import at.ac.tuwien.sbc.valesriegler.group.actions.TableFree;
import at.ac.tuwien.sbc.valesriegler.group.actions.TableRequest;
import at.ac.tuwien.sbc.valesriegler.group.actions.TableResponse;
import at.ac.tuwien.sbc.valesriegler.pizzeria.AbstractPizzeriaConnector;
import at.ac.tuwien.sbc.valesriegler.pizzeria.PizzeriaAgent;
import at.ac.tuwien.sbc.valesriegler.pizzeria.actions.TableNew;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;
import at.ac.tuwien.sbc.valesriegler.types.Table;
import at.ac.tuwien.sbc.valesriegler.waiter.actions.DeliverOrder;

/**
 * This class handles the communication with other processes using JMS.
 * 
 * @author jan
 * 
 */
public class JMSPizzeriaConnector extends AbstractPizzeriaConnector implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(JMSPizzeriaConnector.class);
	private final String CONNECTSTRING;
	private Connection connection;

	public JMSPizzeriaConnector(String jmsURL) {
		CONNECTSTRING = jmsURL;
	}

	@Override
	public void init() {
		try {
			connection = new ActiveMQConnectionFactory(CONNECTSTRING).createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consumer = session.createConsumer(session.createQueue("PizzeriaConnector"));
			consumer.setMessageListener(this);
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}

	@Override
	public void onMessage(Message msg) {
		try {
			msg.acknowledge();
			if (msg instanceof ObjectMessage) {
				ObjectMessage objMsg = (ObjectMessage) msg;
				Object obj = objMsg.getObject();

				if (obj instanceof TableRequest) {
					TableRequest tablerrquest = (TableRequest) obj;
					log.debug("Received: " + tablerrquest);
					ArrayList<GroupData> ngroup = new ArrayList<GroupData>();
					ngroup.add(tablerrquest.getGroupdata());
					PizzeriaAgent.getInstance().getGroupModel().addItems(ngroup);
					PizzeriaAgent.getInstance().getGroupModel().fireTableDataChanged();
					return;

				} else if (obj instanceof TableResponse) {
					TableResponse tablerresponse = (TableResponse) obj;
					log.debug("Received: " + tablerresponse);
					Table t = PizzeriaAgent.getInstance().getTablesModel().getTableById(tablerresponse.getTable().getId());
					t.setGroupId(tablerresponse.getGroupdata().getId());
					PizzeriaAgent.getInstance().getTablesModel().fireTableDataChanged();
					PizzeriaAgent.getInstance().getGroupModel().removeGroup(tablerresponse.getGroupdata().getId());
					PizzeriaAgent.getInstance().getGroupModel().fireTableDataChanged();
					return;

				} else if (obj instanceof OrderResponse) {
					OrderResponse orderResponse = (OrderResponse) obj;
					log.debug("Received: " + orderResponse);
					orderResponse.getGroupdata().setOrderWaiter(orderResponse.getWaiterId());
					log.info("order: " + orderResponse);
					PizzeriaAgent.getInstance().getOrdersModel().updateGroupData(orderResponse.getGroupdata());
					PizzeriaAgent.getInstance().getOrdersModel().fireTableDataChanged();
					return;

				} else if (obj instanceof OrderInfo) {
					OrderInfo orderInfo = (OrderInfo) obj;
					log.debug("Received: " + orderInfo);
					PizzeriaAgent.getInstance().getOrdersModel().updateGroupData(orderInfo.getGroupdata());
					PizzeriaAgent.getInstance().getOrdersModel().fireTableDataChanged();
					return;

				} else if (obj instanceof DeliverOrder) {
					DeliverOrder deliverOrder = (DeliverOrder) obj;
					log.debug("Received: " + deliverOrder);
					deliverOrder.getGroupdata().setServingWaiter(deliverOrder.getWaiterId());
					PizzeriaAgent.getInstance().getOrdersModel().updateGroupData(deliverOrder.getGroupdata());
					return;

				} else if (obj instanceof PayResponse) {
					PayResponse payResponse = (PayResponse) obj;
					log.debug("Received: " + payResponse);
					payResponse.getGroupdata().setPayingWaiter(payResponse.getWaiterId());
					PizzeriaAgent.getInstance().getOrdersModel().updateGroupData(payResponse.getGroupdata());
					return;

				} else if (obj instanceof TableFree) {
					TableFree tablefree = (TableFree) obj;
					log.debug("Received: " + tablefree);
					int tid = tablefree.getGroupdata().getTable().getId();
					Table t = PizzeriaAgent.getInstance().getTablesModel().getTableById(tid);
					t.setGroupId(-1);
					PizzeriaAgent.getInstance().getTablesModel().fireTableDataChanged();
					return;

				} else if (obj instanceof DeliveryOrderResponse) {
					DeliveryOrderResponse deliveryOrderResponse = (DeliveryOrderResponse) obj;
					log.debug("Received: " + deliveryOrderResponse);
					deliveryOrderResponse.getDeliveryGroupData().setWaiterIdOfOrder(deliveryOrderResponse.getWaiterId());
					ArrayList gd = new ArrayList();
					gd.add(deliveryOrderResponse.getDeliveryGroupData());
					PizzeriaAgent.getInstance().getDeliveryOrdersModel().addItems(gd);
					return;

				} else if (obj instanceof DeliveryOrderInfo) {
					DeliveryOrderInfo deliveryOrderInfo = (DeliveryOrderInfo) obj;
					log.debug("Received: " + deliveryOrderInfo);
					ArrayList gd = new ArrayList();
					gd.add(deliveryOrderInfo.getDeliveryGroupData());
					PizzeriaAgent.getInstance().getDeliveryOrdersModel().addItems(gd);
					return;

				} else if (obj instanceof OrderDeliveredInfo) {
					OrderDeliveredInfo orderDeliveredInfo = (OrderDeliveredInfo) obj;
					log.debug("Received: " + orderDeliveredInfo);
					orderDeliveredInfo.getDeliveryGroupData().setDriverId(orderDeliveredInfo.getDriverId());
					ArrayList gd = new ArrayList();
					gd.add(orderDeliveredInfo.getDeliveryGroupData());
					PizzeriaAgent.getInstance().getDeliveryOrdersModel().addItems(gd);
					return;

				}
				log.warn("Unknown message received!" + obj);
			}

			log.warn("Unknown messagetype received!");
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}
	@Override
	public void send(AbstractAction request) {
		try {
			if (request instanceof TableNew) {

				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				MessageProducer wantToSitAtTable = session.createProducer(session.createQueue("TablesFree"));
				wantToSitAtTable.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				wantToSitAtTable.send(session.createObjectMessage(((TableNew) request).getTable()));
				session.close();
				return;
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		}
	}

	public String getCONNECTSTRING() {
		return CONNECTSTRING;
	}

}
