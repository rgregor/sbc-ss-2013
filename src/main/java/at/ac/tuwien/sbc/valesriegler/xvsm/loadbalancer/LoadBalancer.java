package at.ac.tuwien.sbc.valesriegler.xvsm.loadbalancer;


import at.ac.tuwien.sbc.valesriegler.common.Tuple;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.xvsm.LoadBalancerXVSM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadBalancer {
    private static final String USAGE = "LoadBalancer needs exactly two integer parameters: LOADBALANCERID, LOADBALANCER-SPACE-PORT";
    private static final Logger log = LoggerFactory.getLogger(LoadBalancer.class);

    private final int pizzeriaPort;
    private final int id;
    private LoadBalancerXVSM xvsm;

    public static void main(String[] args) {
        final Tuple<Integer> loadBalancerIdAndSpacePort = Util.parseIdAndSpacePort(args, USAGE);

        LoadBalancer loadBalancer = new LoadBalancer(loadBalancerIdAndSpacePort.fst, loadBalancerIdAndSpacePort.snd);
        loadBalancer.start();
    }

    private void start() {
        xvsm = new LoadBalancerXVSM(id, pizzeriaPort);

        xvsm.listenForPizzerias();
    }

    public LoadBalancer(int id, int pizzeriaPort) {
        this.id = id;
        this.pizzeriaPort = pizzeriaPort;
        log.info("I AM A Loadbalancer WITH ID {}", id);
    }
}
