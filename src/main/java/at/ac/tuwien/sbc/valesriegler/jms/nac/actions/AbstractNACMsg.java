package at.ac.tuwien.sbc.valesriegler.jms.nac.actions;

import java.io.Serializable;

/**
 * Abstract NAC Message.
 * 
 * @author jan
 * 
 */
public abstract class AbstractNACMsg implements Serializable {
	public AbstractNACMsg() {
	}
}
