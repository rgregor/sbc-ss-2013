package at.ac.tuwien.sbc.valesriegler.waiter.jms;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.HasId;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.jms.nac.JMSNAC;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners.CookedOrders;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners.WantADelivery;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners.WantToOrder;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners.WantToPay;
import at.ac.tuwien.sbc.valesriegler.waiter.jms.messageListeners.WantToSitAtTable;

/**
 * A waiter is a person who does all the stuff and gets lousy tips in return.
 * TODO: might want to split this class into multiple, to allow for abstraction.
 * 
 * @author jan
 * 
 */
public class JMSWaiter implements HasId {
	private static final Logger log = LoggerFactory.getLogger(JMSWaiter.class);
	private final String CONNECTSTRING;
	private Connection connection;

	public String getCONNECTSTRING() {
		return CONNECTSTRING;
	}

	final private int id;
	private JMSNAC jmsnac;

	public JMSWaiter(String jmsURL, int id) {
		CONNECTSTRING = jmsURL;
		this.id = id;
		log.info("I AM A WAITER WITH ID {}", id);

		WaiterJMSNACMsgListener tmp = new WaiterJMSNACMsgListener();
		jmsnac = new JMSNAC(tmp);

		if (!Util.runSimulation) {
			initJMS();
		}
	}

	public void initJMS() {
		try {
			// Connecting to the Broker and to the output queue
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(CONNECTSTRING);
			connection = connectionFactory.createConnection();
			connection.start();

			Session sessWantToSit = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consWantToSit = sessWantToSit.createConsumer(sessWantToSit.createQueue("WantToSitAtTable"));
			consWantToSit.setMessageListener(new WantToSitAtTable(this));

			Session sessWantToOrder = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consWantToOrder = sessWantToOrder.createConsumer(sessWantToOrder.createQueue("WantToOrder"));
			consWantToOrder.setMessageListener(new WantToOrder(this));

			Session sessCookedOrders = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consCookedOrders = sessCookedOrders.createConsumer(sessCookedOrders.createQueue("CookedOrders"));
			consCookedOrders.setMessageListener(new CookedOrders(this));

			Session sessWantToPay = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consWantToPay = sessWantToPay.createConsumer(sessWantToPay.createQueue("WantToPay"));
			consWantToPay.setMessageListener(new WantToPay(this));

			Session sessWantADelivery = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageConsumer consWantADelivery = sessWantADelivery.createConsumer(sessWantADelivery
					.createQueue("WantADelivery"));
			consWantADelivery.setMessageListener(new WantADelivery(this));

		} catch (Exception e) {
			log.error("Caught: ", e);
		}
	}

	@Override
	public String toString() {
		return "Waiter [id=" + id + "]";
	}

	@Override
	public int getId() {
		return id;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
}
