package at.ac.tuwien.sbc.valesriegler.common;

import java.io.Serializable;

public class OrderId implements Serializable {
    private final Integer id;

    public OrderId(Integer id) {
        this.id = id;
    }

    public int getId() {
        return Util.getIntSafe(id);
    }
}
