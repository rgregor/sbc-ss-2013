package at.ac.tuwien.sbc.valesriegler.pizzeria.gui.tablemodels;


import at.ac.tuwien.sbc.valesriegler.common.TableModel;
import at.ac.tuwien.sbc.valesriegler.types.HasOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractOrdersModel<T extends HasOrder> extends TableModel<T> {
    private static final Logger log = LoggerFactory.getLogger(AbstractOrdersModel.class);

    public T getHasOrderById(int id) {
        return items.get(id);
    }
}
