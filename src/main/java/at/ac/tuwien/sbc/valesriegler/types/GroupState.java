package at.ac.tuwien.sbc.valesriegler.types;

import java.io.Serializable;

/**
 * Enum denoting all the possible states a Group can be in.
 * 
 * @author jan
 * 
 */
public enum GroupState implements Serializable {
	NEW, WAITING, SITTING, ORDER_PENDING, ORDERED, EATING, PAY, GONE
}
