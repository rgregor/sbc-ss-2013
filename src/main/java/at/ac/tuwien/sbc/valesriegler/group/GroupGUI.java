package at.ac.tuwien.sbc.valesriegler.group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.group.gui.GroupFrame;

public class GroupGUI implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(GroupAgent.class);
	private GroupFrame frame;
	@Override
	public void run() {
		log.debug("Starting GroupGUI.");
		frame = new GroupFrame(GroupAgent.getInstance().getGroupModel(), GroupAgent.getInstance().getDeliveryModel());
		frame.pack();
		frame.setVisible(true);
	}
	public GroupFrame getFrame() {
		return frame;
	}
}