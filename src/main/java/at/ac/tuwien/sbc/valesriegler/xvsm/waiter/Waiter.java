package at.ac.tuwien.sbc.valesriegler.xvsm.waiter;

import at.ac.tuwien.sbc.valesriegler.common.Tuple;
import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.xvsm.WaiterXVSM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * This is a waiter using XVSM
 * 
 * @author Gregor Riegler <gregor DOT riegler AT gmail DOT com>
 * 
 */
public class Waiter implements Serializable {
    private static final String USAGE = "Waiter needs exactly two integer parameters: WAITERID, PIZZERIA-SPACE-PORT";
	private static final Logger log = LoggerFactory.getLogger(Waiter.class);

    private final int port;
    private final int id;
	private WaiterXVSM xvsm;

	public static void main(String[] args) {
        final Tuple<Integer> waiterIdAndSpacePort = Util.parseIdAndSpacePort(args, USAGE);

		Waiter waiter = new Waiter(waiterIdAndSpacePort.fst, waiterIdAndSpacePort.snd);
		waiter.start();
	}

	private void start() {
		xvsm = new WaiterXVSM(id, port);
		
		xvsm.listenForOrderRequests();
		
		xvsm.listenForPaymentRequest();
		
		xvsm.listenForPreparedPizzas();
		
		// when new guests arrive the waiter should try to assign a table to them
		xvsm.listenForNewGuests();
		
		// when tables get free the waiter should have a look if there are waiting guests and assign the new table to them
		xvsm.listenForFreeTable();

        xvsm.listenForPhoneOrders();
	}


	public Waiter(int id, int port) {
		this.id = id;
        this.port = port;
		log.info("I AM A WAITER WITH ID {}", id);
	}

}
