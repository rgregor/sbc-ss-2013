package at.ac.tuwien.sbc.valesriegler.jms.nac.actions;

/**
 * Message hat instructs everyone to reply their Address.
 * 
 * @author jan
 * 
 */
public class BenchmarkStop extends AbstractNACMsg {
	public BenchmarkStop() {
	}

	@Override
	public String toString() {
		return "BenchmarkStop []";
	}
}
