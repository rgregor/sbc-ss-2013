package at.ac.tuwien.sbc.valesriegler.common;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import javax.swing.table.AbstractTableModel;

/**
 * Common base class of table models used in Group GUI and Pizzeria GUI.
 * 
 * @author Gregor Riegler <gregor DOT riegler AT gmail DOT com>
 *
 */
public abstract class TableModel<Item extends HasId> extends AbstractTableModel {
	protected static final String UNHANDLEDCOLUMN = "Unhandled column";
	
	protected final Map<Integer, Item> items = new ConcurrentSkipListMap<Integer, Item>();
	
	public void addItems(List<Item> newItems) {
		for (Item item : newItems) {
			items.put(item.getId(), item);
		}
		
		fireTableDataChanged();
	}
	
	public void setItems(List<Item> newItems) {
		items.clear();
		addItems(newItems);
	}
	
	@Override
	public int getRowCount() {
		return items.size();
	}
	
	@Override
	public String getColumnName(int column) {
		return getColumns()[column];
	}

	@Override
	public int getColumnCount() {
		return getColumns().length;
	}
	
	protected abstract String[] getColumns();
}
