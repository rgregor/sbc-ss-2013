package at.ac.tuwien.sbc.valesriegler.common;

import at.ac.tuwien.sbc.valesriegler.types.DeliveryGroupData;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * Abstract delivery action that can be performed in the DS. In almost all cases the
 * DeliveryGroupData is relevant, it may be null in the few corner cases.
 * 
 * @author jan
 * 
 */
public abstract class AbstractDeliveryAction extends AbstractAction {
	private final DeliveryGroupData groupData;

	public AbstractDeliveryAction(DeliveryGroupData groupData) {
		super(null);
		this.groupData = groupData;
	}
	/**
	 * do not use.
	 * 
	 * @return
	 */
	@Deprecated
	@Override
	public GroupData getGroupdata() {
		throw new IllegalArgumentException("use getDeliveryGroupData() instead!");
	}

	public DeliveryGroupData getDeliveryGroupData() {
		return groupData;
	}
}