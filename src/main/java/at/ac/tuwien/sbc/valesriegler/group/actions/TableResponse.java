package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;
import at.ac.tuwien.sbc.valesriegler.types.Table;

/**
 * Response to the wish of getting a table to sitdown.
 * 
 * @author jan
 * 
 */
public class TableResponse extends AbstractAction implements Serializable {
	private final Table table;
	private final int waiterId;

	public TableResponse(GroupData groupdata, Table table, int waiterId) {
		super(groupdata);
		this.table = table;
		this.waiterId = waiterId;
	}

	public Table getTable() {
		return table;
	}

	public int getWaiterId() {
		return waiterId;
	}

	@Override
	public String toString() {
		return "TableResponse [table=" + table + ", waiterId=" + waiterId + "]";
	}

}
