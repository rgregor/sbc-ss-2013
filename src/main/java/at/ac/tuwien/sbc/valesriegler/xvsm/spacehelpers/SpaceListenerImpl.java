package at.ac.tuwien.sbc.valesriegler.xvsm.spacehelpers;

import org.mozartspaces.capi3.AnyCoordinator;
import org.mozartspaces.core.Capi;
import org.mozartspaces.core.ContainerReference;
import org.mozartspaces.core.MzsConstants;
import org.mozartspaces.core.MzsConstants.RequestTimeout;
import org.mozartspaces.notifications.NotificationManager;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Gregor Riegler <gregor DOT riegler AT gmail DOT com>
 *
 * @see SpaceListener
 */
public class SpaceListenerImpl extends SpaceListener {

    public final NotificationManager notificationManager;
    private Capi capi;
	private ContainerReference cref;
    private final boolean countAll;


    public SpaceListenerImpl(Capi capi, ContainerReference cref, int timeout, SpaceAction spaceAction, NotificationManager notificationManager, boolean lookaround, boolean countAll, String name, boolean noNotification) {
        this.capi = capi;
        this.cref = cref;
        this.timeout = timeout;
        this.spaceAction = spaceAction;
        this.notificationManager = notificationManager;
        this.lookAround = lookaround;
        this.countAll = countAll;
        this.name = name;
        this.noNotification = noNotification;
    }

    @Override
	public List<? extends Serializable> getEntries() throws Exception {
        final int count = countAll ? MzsConstants.Selecting.COUNT_ALL : MzsConstants.Selecting.COUNT_MAX;
        return capi.read(cref, AnyCoordinator
                .newSelector(count),
                RequestTimeout.DEFAULT, null);
	}
}
