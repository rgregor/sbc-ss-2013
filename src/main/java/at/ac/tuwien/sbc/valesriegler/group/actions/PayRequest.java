package at.ac.tuwien.sbc.valesriegler.group.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * indicate a groups interest to pay and leave.
 * 
 * @author jan
 * 
 */
public class PayRequest extends AbstractAction implements Serializable {
	public PayRequest(GroupData groupdata) {
		super(groupdata);
	}

	@Override
	public String toString() {
		return "PayRequest []";
	}

}
