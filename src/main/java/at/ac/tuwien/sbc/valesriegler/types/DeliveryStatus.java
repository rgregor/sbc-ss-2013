package at.ac.tuwien.sbc.valesriegler.types;

import java.io.Serializable;

public enum DeliveryStatus implements Serializable {
	ORDERED, MOVED, IN_PROGRESS, IS_DELIVERED, DELIVERED, PAID, DELIVERY_FAILED, START, ORDER_PENDING;
}
