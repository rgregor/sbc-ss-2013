package at.ac.tuwien.sbc.valesriegler.waiter.actions;

import java.io.Serializable;

import at.ac.tuwien.sbc.valesriegler.common.AbstractAction;
import at.ac.tuwien.sbc.valesriegler.types.GroupData;

/**
 * bring the order to the group.
 * 
 * @author jan
 * 
 */
public class DeliverOrder extends AbstractAction implements Serializable {
	private final int waiterId;

	public DeliverOrder(GroupData groupdata, int waiterId) {
		super(groupdata);
		this.waiterId = waiterId;
	}

	public int getWaiterId() {
		return waiterId;
	}

	@Override
	public String toString() {
		return "DeliverOrder [waiterId=" + waiterId + "]";
	}

}
