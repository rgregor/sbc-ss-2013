package at.ac.tuwien.sbc.valesriegler.driver.jms.messageListeners;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.ac.tuwien.sbc.valesriegler.common.Util;
import at.ac.tuwien.sbc.valesriegler.cook.actions.DeliveryOrderInfo;
import at.ac.tuwien.sbc.valesriegler.driver.actions.OrderDeliveredInfo;
import at.ac.tuwien.sbc.valesriegler.driver.jms.JMSDriver;
import at.ac.tuwien.sbc.valesriegler.types.DeliveryStatus;

/**
 * 
 * 
 * @author jan
 * 
 */
public class CookedDeliveryOrders implements MessageListener {
	private static final Logger log = LoggerFactory.getLogger(CookedDeliveryOrders.class);
	private final JMSDriver driver;

	public CookedDeliveryOrders(JMSDriver driver) {
		this.driver = driver;
	}

	@Override
	public void onMessage(Message msg) {
		try {
			synchronized (driver) {
				if (msg instanceof ObjectMessage) {
					ObjectMessage objMsg = (ObjectMessage) msg;
					Object obj = objMsg.getObject();

					if (obj instanceof DeliveryOrderInfo) {
						DeliveryOrderInfo doi = (DeliveryOrderInfo) obj;
						log.debug("Received: " + doi);

						// generate random delay
						if (!Util.runSimulation) {
							Thread.sleep((long) (Math.random() * 10000));
						}
						String addr = doi.getDeliveryGroupData().getAddress();
						boolean delivered = false;
						OrderDeliveredInfo odi = new OrderDeliveredInfo(doi.getDeliveryGroupData(), driver.getId());

						// Deliver to destination broker
						if (!Util.runSimulation) {
							try {
								ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
										Util.JMS_DELIVERY_DESTINATION);
								Connection connection = connectionFactory.createConnection();
								connection.start();
								log.debug("CookedDelivery:ActiveMQConnectionFactory::" + connection);

								Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
								MessageProducer prod = session.createProducer(session.createQueue(addr));
								prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
								odi.getDeliveryGroupData().setDeliveryStatus(DeliveryStatus.DELIVERED);
								prod.send(session.createObjectMessage(odi));
								session.close();
								connection.close();
								delivered = true;
							} catch (JMSException e) {
								log.error("EXCEPTION!", e);
							}
						}

						if (!delivered) {
							odi.getDeliveryGroupData().setDeliveryStatus(DeliveryStatus.DELIVERY_FAILED);
						}

						// Inform pizzeria
						ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(driver.getCONNECTSTRING());
						Connection connection = connectionFactory.createConnection();
						connection.start();

						Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
						MessageProducer prod = session.createProducer(session.createQueue("GroupConnector"));
						prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
						prod.send(session.createObjectMessage(odi));
						session.close();

						if (!Util.runSimulation) {
							session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
							prod = session.createProducer(session.createQueue("PizzeriaConnector"));
							prod.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
							prod.send(session.createObjectMessage(odi));
							session.close();
							connection.close();
						}
					} else {
						log.warn("Received unknown Object: " + obj);
					}
				} else {
					log.warn("Received unknown Message: " + msg);
				}
				msg.acknowledge();
			}
		} catch (JMSException e) {
			log.error("EXCEPTION!", e);
		} catch (InterruptedException e) {
			log.error("EXCEPTION!", e);
		}
	}

	@Override
	public String toString() {
		return "PendingDeliveries [driver=" + driver + "]";
	}
}
