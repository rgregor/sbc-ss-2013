#!/bin/bash
mvn package

mvn exec:java -Dexec.mainClass="at.ac.tuwien.sbc.valesriegler.group.GroupAgent" -Dexec.args="JMS tcp://localhost:61610?jms.prefetchPolicy.all=1" &